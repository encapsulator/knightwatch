package Entity.Command;

import Entity.MapEntity;
import Main.Settings;

public class SlowDown implements Command {

	@Override
	public void execute(MapEntity e) {
		// TODO Auto-generated method stub
		
		if (e.direction.x!=0) {
			e.direction.x *= Settings.FRICTION;
		}
		
	}

}
