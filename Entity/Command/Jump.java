package Entity.Command;

import Entity.MapEntity;
import Main.DiskJockey;
import Main.Settings;

public class Jump implements Command {

	@Override
	public void execute(MapEntity e) {
		// TODO Auto-generated method stub
		try {
			DiskJockey.playClip(DiskJockey.JUMP, false);

		} catch (Exception e1) {
			e1.printStackTrace();
		}
		e.direction.y = -Settings.JUMPPUSH;
		e.dubbleJump = true;
		e.onFloor = false;
		e.jumpsLeft-=1;
		e.inAir = true;
	} 
	
}
