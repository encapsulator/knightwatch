package Entity.Command;

import Entity.MapEntity;


public interface Command {
	
	void execute(MapEntity e);
	
}
