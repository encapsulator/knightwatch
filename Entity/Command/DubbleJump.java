package Entity.Command;

import Entity.MapEntity;
import Main.DiskJockey;
import Main.Settings;

public class DubbleJump implements Command {

	@Override
	public void execute(MapEntity e) {
		// TODO Auto-generated method stub
		try {
			DiskJockey.playClip(DiskJockey.JUMP, false);

		} catch (Exception e1) {
			e1.printStackTrace();
		}
		e.dubbleJump = true;
		e.jumpsLeft -= 1;
		e.direction.y = -Settings.JUMPPUSH / Settings.DBLJUMPFACTOR;
		
	}

}
