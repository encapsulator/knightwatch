package Entity.Command;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import Entity.MapEntity;
import Main.DiskJockey;

public class Attack implements Command {


	@Override
	public void execute(MapEntity e) {
		// TODO Auto-generated method stub

		try {
			DiskJockey.playClip(DiskJockey.ATTACK, false);
		} catch (IOException | UnsupportedAudioFileException
				| LineUnavailableException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		e.isAttacking = true;

	}

}
