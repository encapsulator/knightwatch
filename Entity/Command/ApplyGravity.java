package Entity.Command;

import Entity.MapEntity;

public class ApplyGravity implements Command {

	@Override
	public void execute(MapEntity e) {
		// TODO Auto-generated method stub

		e.direction.y += e.gravity;
		e.direction.y = e.direction.y <= e.maxY?e.direction.y:e.maxY;
	}

}
