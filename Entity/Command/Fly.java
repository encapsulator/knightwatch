package Entity.Command;

import Entity.MapEntity;
import Main.Settings;

public class Fly implements Command {

	@Override
	public void execute(MapEntity e) {
		// TODO Auto-generated method stub
		e.direction.y += Settings.SPEEDY;
		if (e.direction.y > Settings.MAXFLYFALLSPEED) {
			e.direction.y = Settings.MAXFLYFALLSPEED;
		}
	}

}
