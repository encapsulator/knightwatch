package Entity.Command;

import Entity.MapEntity;
import Main.Settings;

/**
 * Created by niels on 14/04/2017.
 */
public class MoveLeft implements Command {
    @Override
    public void execute(MapEntity e) {

        if(!e.isDefending) {
            if(e.isCrouching) {
                e.direction.x = -Settings.MINX/Settings.MAXCROUCHSPEEDDIVISOR;
            } else {
                e.direction.x = -Settings.MINX;
            }
        }
    }
}
