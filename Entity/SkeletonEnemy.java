package Entity;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import Main.DiskJockey;

import javax.imageio.ImageIO;

import Entity.Command.ApplyGravity;
import Entity.Observer.InAirObserver;
import Main.ImageLoader;
import Main.Settings;
import Shape.Rectangle;
import Shape.Shape;
import TileMap.TileMap;

import static Main.Settings.*;

public class SkeletonEnemy extends MapEntity {
	private BufferedImage idleImage;
	private double attackWithin = 0;
	private int timesAttacked = 0;
	private boolean isMoving = false;
	private boolean isDying = false, isDead, isAttacking, isCharging, reverseAttack=false;
	private double flyBackOnHitWidth = FLYBACKWIDTH;
	private double flyBackProduct = 5;
	private double hitOnShieldTimeout = HITONSHIELDTIMEOUT;


	private boolean showDeadBody = true;
	private double dieTimeout = DIETIMEOUT;
	private double disappearAfterDeadTimeout = DISAPPEARAFTERDEADTIMEOUT;
	private int attackDelay;
	private Player player;
	public SkeletonEnemy(double x, double y, TileMap t) {
		super(t);
		isDead = false;
		shape = new Rectangle(x, y, 50, 100);
		direction.x = direction.y = 0;
		inAir = true;
		animationManager = new AnimationManager();
		load();
	}
	
	public void setPlayer(Player player){
		this.player = player;
	}

	public void load() {
		try {

			animationManager.registerAnimation("attacking",Settings.SKELETON_ATTACK,1,4, false);
			animationManager.registerAnimation("charging",Settings.SKELETON_CHARGE,1,3, true);
			animationManager.registerAnimation("dying",Settings.SKELETON_DIE,1,4, true);
			idleImage = ImageIO.read(getClass()
					.getResourceAsStream(Settings.SKELETON_IDLE));
			idleImage = ImageLoader.getInstance().loadImage(idleImage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		isCharging = false;
		addCommand("gravity", new ApplyGravity());
		super.attach(new InAirObserver(this));
		attackDelay = Settings.ATTACKDELAY;

	}

	public void draw(Graphics2D g) {
		if (isVisible() && !isDead) {
			if (!isMoving && !isAttacking && !isCharging && !isDying) {
				g.drawImage(idleImage, (int) shape.position.x - 58,
						(int) shape.position.y - 23, null);
			} else if(isDying) {
					dieTimeout--;
					if(dieTimeout>0) {
						g.drawImage(animationManager.getAnimations().get("dying").getImage(),(int) shape.position.x - 58,
								(int) shape.position.y - 23, null);
					} else {
						disappearAfterDeadTimeout--;

						if(disappearAfterDeadTimeout%DEADSWITCHSHOWTIMEOUT == 0){
							showDeadBody = !showDeadBody;
						}
						if(showDeadBody) {
							g.drawImage(animationManager.getAnimations().get("dying").getImage(),(int) shape.position.x - 58,
									(int) shape.position.y - 23, null);
						}
						if(disappearAfterDeadTimeout<=0){
							isDead = true;
							isDying = false;
							disappearAfterDeadTimeout = DISAPPEARAFTERDEADTIMEOUT;
							dieTimeout = DIETIMEOUT;
						}
					}


			} else if(isCharging){
				g.drawImage(animationManager.getAnimations().get("charging").getImage(),(int) shape.position.x - 58,
						(int) shape.position.y - 25, null);
			} else if(isAttacking){
				g.drawImage(animationManager.getAnimations().get("attacking").getImage(),(int) shape.position.x - 58,
						(int) shape.position.y - 23, null);
			}
		}
	}

	public void update() {
		notifyObservers();

		double playerdx = player.direction.x;
		double playerdy = player.direction.y;
		direction.x=0;
		if(!isDying && !isDead) {
			ArtificialIntelligence(player.getShape().position.x, player.getShape().position.y, playerdx, playerdy);
			checkIfHit();
		}
		if(isDying) {
			flyBackOnHitWidth-= HITBOXWIDTH/FLYBACKTIME;

			if(flyBackOnHitWidth>0){
				flyBackProduct -= FLYBACKDECELERATE;
				flyBackProduct = flyBackProduct<=0?0:flyBackProduct;
				direction.x += ((HITBOXWIDTH/FLYBACKTIME) * (flyBackProduct));
			} else {
				animationManager.getAnimations().get("dying").setDelay(5);
				animationManager.getAnimations().get("dying").update();
			}

		}
		else if (isAttacking) {
			if(!reverseAttack) {
				animationManager.getAnimations().get("attacking").update();
			} else {
				hitOnShieldTimeout--;
				if(hitOnShieldTimeout<=0) {
					isAttacking=false;
					attackDelay = ATTACKDELAY;
					reverseAttack=false;
					hitOnShieldTimeout = HITONSHIELDTIMEOUT;
					Animation attacking = animationManager.getAnimations().get("attacking");
					attacking.setTimesPlayed(attacking.getNumPlayed()+1);
				}
			}

		}
		else if (isCharging) {
			animationManager.getAnimations().get("charging").update();
		}

		boolean res = checkTileMapCollission();
		if(res) {
			inAir=false;
			onFloor=true;
		}

		shape.translate(direction);
	}

	private void checkIfHit() {
		if(player.isAttacking && (player.getAnimationManager().getAnimations().get("attacking").getFrame()==6 || (player.inAir && player.getAnimationManager().getAnimations().get("jumpAttacking").getFrame()==3))) {
			checkDamage();
		}
	}

	private void ArtificialIntelligence(double playerX, double playerY, double dx, double dy) {

		attackDelay--;
		if(attackDelay <= 0 && playerX >= shape.position.x -300 && !isCharging && !isAttacking){
			isCharging = true;
			double distance = shape.position.x-ATTACTMAXCOUNTDOWNTIME - playerX;
			if(dx > 1 || dx <-1){
				attackWithin = distance / Math.abs(dx);
			} else {
				attackWithin = ATTACTMAXCOUNTDOWNTIME;
			}
			attackWithin -=1;
		} else if(isCharging){
			attackWithin -=1;
			if(attackWithin <=0){
				isCharging = false;
				isAttacking = true;
				timesAttacked = animationManager.getAnimations().get("attacking").getNumPlayed()+1;
			}
		} else if(isAttacking){
			if(animationManager.getAnimations().get("attacking").getNumPlayed() ==  timesAttacked){
				isAttacking = false;
				attackDelay = ATTACKDELAY;

			} else if(animationManager.getAnimations().get("attacking").getFrame()==1 && !reverseAttack){
				checkPlayerDamage();
			}
			
		}
	}

	public void checkDamage(){
		//make new rectangle for sword collission
		Shape playerShape = player.getShape();

		Rectangle2D playerRect = new Rectangle2D.Double(playerShape.position.x+player.direction.x, playerShape.position.y+player.direction.y + ((Rectangle) playerShape).height/3, 90, ((Rectangle) playerShape).height/3);
		Rectangle2D enemyRect = new Rectangle2D.Double(shape.position.x, shape.position.y, ((Rectangle) shape).width-20, ((Rectangle) shape).height/2);
		if(playerRect.intersects(enemyRect) && !isDying){
			//enemy is hit
			int product = (int)Math.abs((player.getShape().position.x - shape.position.x)/20);
			flyBackProduct = product >5/2?5:product*2<=0?1:product*2;
			isDying = true;

			try {
				DiskJockey.playClip(DiskJockey.SKELETONHURT, false);
			} catch (Exception e) {
				e.printStackTrace();
			}


		}
	}

	public void checkPlayerDamage(){
		//make new rectangle for sword collission
		Shape playerShape = player.getShape();
	
		Rectangle2D playerRect = new Rectangle2D.Double(playerShape.position.x+player.direction.x, playerShape.position.y+player.direction.y + ((Rectangle) playerShape).height/3, ((Rectangle) playerShape).width, ((Rectangle) playerShape).height/3);
		Rectangle2D enemyRect = new Rectangle2D.Double(shape.position.x-60, shape.position.y, ((Rectangle) shape).width, ((Rectangle) shape).height/2);
		if(playerRect.intersects(enemyRect)){
			//player might have been hit
			if(!player.isDefending && !player.isHurt){
				player.hit(SWORDHITDAMAGE);
			} else if(player.isDefending) {
				reverseAttack=true;
				shape.position.x=((Rectangle) playerShape).getBorder().right+52+(player.direction.x);
			}
		}
	}

	public HashMap<String, Animation> getAnimations() {
		return animationManager.getAnimations();
	}





}
