package Entity;

import Main.Camera;
import Main.ImageLoader;
import Main.Settings;
import Math.Vector;

import java.awt.*;

import static KeyHandler.KeyHandler.*;
import static Main.Settings.*;

/**
 * Created by niels on 20/04/2017.
 */
public class LevelUI extends DynamicEntity {

    private Image statsImage;

    private double statsX = STATSX;
    private double coinTextX = COINSTXTX;
    private double healthTextX = HEALTHTXTX;
    private double fpsTextX = FPSTXTX;
    private int amountCoins = 0;
    private int health = HEALTH;
    private String healthText = "", coinText = "", fpsText="";

    private static LevelUI instance;

    public static LevelUI getInstance() {
        instance=instance==null?new LevelUI():instance;
        return instance;
    }

    public LevelUI(){
        statsImage = ImageLoader.getInstance().loadImage(Settings.LVLSTATS);
    }
    public int getAmountCoins() { return amountCoins; }
    public void setAmountCoins(int amountCoins) {
        this.amountCoins = amountCoins;
    }

    public void setHealth(int health) {
        this.health = health<=0?0:health;
        healthText = health+"";
    }

    public void setFps(int fps) {
        fpsText= fps + " FPS";
    }

    @Override
    public void update() {
        direction = new Vector((int)Camera.scrollPixels, 0);
        statsX = statsX + direction.x;
        statsX=statsX<= STATSX?STATSX:statsX;
        coinTextX = coinTextX + direction.x;
        coinTextX=coinTextX<=COINSTXTX?COINSTXTX:coinTextX;
        healthTextX = healthTextX + direction.x;
        healthTextX=healthTextX<=HEALTHTXTX?HEALTHTXTX:healthTextX;
        fpsTextX = fpsTextX + direction.x;
        fpsTextX=fpsTextX<=FPSTXTX?FPSTXTX:fpsTextX;
        coinText = amountCoins+"";
        healthText = health+"";
    }

    @Override
    public void draw(Graphics2D g) {

        g.drawImage(statsImage, (int)statsX, STATSY, null);
        g.setColor(Color.WHITE);
        g.setFont(new Font("Verdana", Font.BOLD, 16));
        FontMetrics fm = g.getFontMetrics( g.getFont() );
        int width = fm.stringWidth(coinText);
        g.drawString(coinText, (int)coinTextX-width, COINTXTY);
        width = fm.stringWidth(healthText);
        g.drawString(healthText, (int)healthTextX-width, HEALTHTXTY);
        g.setFont(new Font("Verdana", Font.PLAIN, 14));
        if(isPressed(COMMAND))g.drawString(fpsText, (int)fpsTextX, FPSTXTY);
    }

    public void reset() {
        setHealth(HEALTH);
        setAmountCoins(0);
        healthText = health+"";
        coinText = amountCoins+"";
    }

    public void resetPos() {
        statsX = STATSX;
        coinTextX = COINSTXTX;
        healthTextX = HEALTHTXTX;
        fpsTextX = FPSTXTX;
    }
}
