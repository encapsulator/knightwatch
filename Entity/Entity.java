package Entity;

import Entity.Command.Command;
import Main.Drawable;
import Shape.Shape;

import java.util.HashMap;

public abstract class Entity implements Drawable {
	protected Shape shape;

	protected HashMap<String, Command> actions = new HashMap<String, Command>();
	
	public Shape getShape(){return shape;}

}
