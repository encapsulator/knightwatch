/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.awt.Graphics2D;
import java.util.*;

import Main.Settings;
import Shape.Circle;
import Entity.Command.Command;
import Entity.Observer.Observer;
import Main.Camera;

import Shape.Rectangle;
import Shape.Shape;
import Shape.Triangle;
import TileMap.TileMap;
import TileMap.Tile;

/**
 *
 * @author niels
 */
public abstract class MapEntity extends DynamicEntity {

	protected Collection<Observer> observers;
	protected ArrayList<Shape> tilesAround;
	protected TileMap tilemap;
	protected int[][][] collissionMap;
	public int jumpsLeft;
	public boolean inAir, dubbleJump, onFloor, isDead, isAttacking,
			isDefending, paralize, isCrouching, isDying, isHurt, isCrouchWalking;
	protected AnimationManager animationManager;

	public MapEntity(TileMap t) {
		tilemap = t;
		observers = new LinkedList<>();
	}

	public void addCommand(String action, Command e) {
		actions.put(action, e);
	}

	public void executeCommand(String action) {
		actions.get(action).execute(this);
	}
	
	public void attach(Observer o) {
		observers.add(o);
	}

	public void notifyObservers() {
		for (Observer o : observers) {
			o.update(this);
		}
	}

	public AnimationManager getAnimationManager(){return animationManager;}
	
	public void updateAnimation(Animation a) {
		a.update();
	}

	public MapEntity() {
	}

	@Override
	public void draw(Graphics2D g) {
		tilemap.draw(g);
	}

	public Shape getShape() {
		return shape;
	}
	//calculate which tiles are near
	public void composeCollissionMap(int r, int c, int[] playerPos,
			int[] posPlayerColMap) {
		collissionMap = new int[r][c][2];
		collissionMap[posPlayerColMap[0]][posPlayerColMap[1]][0] = playerPos[1];
		collissionMap[posPlayerColMap[0]][posPlayerColMap[1]][1] = playerPos[0];

		for (int rows = 0; rows != r; rows++) {
			for (int cols = 0; cols != c; cols++) {
				int RowDif = -(posPlayerColMap[0] - rows);
				int ColDif = -(posPlayerColMap[1] - cols);

				collissionMap[rows][cols][0] = playerPos[1] + RowDif;
				collissionMap[rows][cols][1] = playerPos[0] + ColDif;
			}
		}
	}
	//Calculate location in map and load surrounding tile objects
	public void loadSurroundingTiles(double x, double y) {
		int tileSize = tilemap.getTileSize();

		int row = (int) Math.floor(x / tileSize);
		int col = (int) Math.floor(y / tileSize);

		int[] posInCollissionMap = new int[2], playerPos = new int[2];
		playerPos[0] = row;
		playerPos[1] = col;
		collissionMap = new int[(int) (((Rectangle)shape).height / tileSize) + 4][(int) (
				((Rectangle)shape).width / tileSize) + 4][2];

		posInCollissionMap[0] = 2;
		posInCollissionMap[1] = 2;
		this.composeCollissionMap(collissionMap.length,
				collissionMap[0].length, playerPos, posInCollissionMap);
	}
	//Check for collission
	public void setSurroundingTiles(double x, double y) {
		loadSurroundingTiles(x, y);
		int[][] map = tilemap.getMap();
		tilesAround = new ArrayList<>();
		int[] numTiles = tilemap.getNumTiles();
		int maxrows = numTiles[0];
		int maxcols = numTiles[1];
		int valX, valY;
		int tileSize = tilemap.getTileSize();

		for (int row = 0; row < collissionMap.length; row++) {
			for (int col = 0; col < collissionMap[0].length; col++) {
				valX = collissionMap[row][col][0];
				valY = collissionMap[row][col][1];
				int initX = valX;
				int initY = valY;
				//make large rectangle shapes from different small BLOCKED tiles;
				if (valX >= 0 && valX < maxrows && valY >= 0 && valY < maxcols) {
					int value = map[valX][valY];
					int rectWidth = 0;
					while (value != 0 && col < collissionMap[0].length
							&& valY < maxcols - 1
							&& tilemap.getType(initX, valY) == Tile.BLOCKED) {
						rectWidth += 1;
						valY++;
						col++;
						value = map[valX][valY];
					}
					if (tilemap.getType(initX, initY) != Tile.BLOCKED) {
						rectWidth = 1;
					} else {
						col--;
					}
					rectWidth *= tileSize;
					if (rectWidth > 0) {
						//add all the tileshapes to the tilesAround map
						if (tilemap.getType(initX, initY) == Tile.BLOCKED) {
							Shape r = new Rectangle((initY * tileSize),
									(initX * tileSize), rectWidth, tileSize);
							tilesAround.add(r);
						} else if (tilemap.getType(initX, initY) == Tile.SLOPEUP) {
							Shape r = new Triangle(initY * tileSize, initX
									* tileSize, rectWidth, tileSize, true);
							tilesAround.add(r);
						} else if (tilemap.getType(initX, initY) == Tile.SLOPEDOWN) {

							Shape r = new Triangle(initY * tileSize, initX
									* tileSize, rectWidth, tileSize, false);
							tilesAround.add(r);
						}
					}
				}

			}
		}
	}
	//check collission
	public boolean checkTileMapCollission() {
		boolean bottomReached = false;
		double xpos = shape.position.x;
		double ypos = shape.position.y;
		setSurroundingTiles(xpos, ypos);
		CollissionTO res;
		ArrayList<Shape> rectangles = new ArrayList<Shape>();
		ArrayList<Shape> shapes = new ArrayList<Shape>();
		for (Shape s : tilesAround) {
			if (s instanceof Rectangle)
				rectangles.add(s);
			if (s instanceof Triangle)
				shapes.add(s);
		}
		shapes.addAll(rectangles);
		for (Shape s : shapes) {

			res = s.accept(shape, direction);
			if (res != null) {
				if (res.y != null) {
					shape.position.y = res.y;

				}
				if (res.x != null) {
					shape.position.x = res.x;
				}
				if (res.dx != null) {
					this.direction.x = res.dx;
				}
				if (res.dy != null) {
					if (!res.inAir) {

						bottomReached = true;
					}
					this.direction.y = res.dy;
				}
				if (res.stopFurtherCollission) {
					break;
				}
			}
		}
		return bottomReached;

	}

	public HashMap<String, Animation> getAnimations() {
		return animationManager.getAnimations();
	}

	public boolean isVisible(){
		if(shape instanceof Rectangle) {
			return shape.position.x+((Rectangle)shape).width>-Camera.scrollX && shape.position.x < -Camera.scrollX + Settings.GAMEWIDTH &&
					shape.position.y +((Rectangle)shape).height > 0 && shape.position.y < Settings.HEIGHT;
		}
		else {
			return shape.position.x+((Circle)shape).radius>-Camera.scrollX && shape.position.x-((Circle)shape).radius < -Camera.scrollX + Settings.GAMEWIDTH &&
					shape.position.y +((Circle)shape).radius > 0 && shape.position.y - ((Circle)shape).radius < Settings.HEIGHT;
		}
	}


}
