package Entity;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by niels on 14/04/2017.
 */
public class CompositeEntity extends MapEntity {

    private Set<MapEntity> entities;

    public CompositeEntity() {
        entities = new HashSet<>();
    }

    public void add(MapEntity object) {
        entities.add(object);
    }

    public Set<MapEntity> getEntities() { return entities; }

    public Set<MapEntity> getSurroundingEntities(double x, double y) {
        return entities.stream().filter(e -> e.isVisible()).collect(Collectors.toSet());
    }

    public void update() {
        entities.stream().filter(x -> x.isVisible()).forEach(x -> x.update());
    }

    public void draw(Graphics2D g) {
        entities.stream().filter(x -> x.isVisible()).forEach(x -> x.draw(g));
    }
}
