package Entity;

import java.awt.Graphics2D;
import java.util.ArrayList;

import Main.*;

public class ParticleEngine extends Thread implements Drawable, Updateable {
	private ArrayList<Particle> particles;

	private final double JMPAMOUNT = 50;
	private final double WINDAMOUNT = 5;
	public final static int JMPPARTICLES = 0;
	public final static int WINDPARTICLES = 1;
	private double[] wind, newWind;
	private double windx = 0, windy = 0;
	private final double MAXWINDX = 2, MINWINDX = -2, MAXWINDY = 1,
			MINWINDY = -1;
	private boolean windSwitch;
	private static ParticleEngine instance;
	private boolean windUpdated = false;

	public ParticleEngine() {
		wind = new double[2];
		newWind = new double[2];
		particles = new ArrayList<>();
		windSwitch = false;
		instance = this;
	}

	public static ParticleEngine getInstance() {
		if (instance == null) {
			instance = new ParticleEngine();
		}
		return instance;
	}

	public void changeWind() {
		windSwitch = !windSwitch;
		if (windSwitch) {
			newWind[0] = Math.random() * (MAXWINDX - MINWINDX) + MINWINDX;
			windx = (newWind[0] - wind[0]) / 30;
			windy = -10;
		} else {
			newWind[1] = Math.random() * (MAXWINDY - MINWINDY) + MINWINDY;
			windx = -10;
			windy = (newWind[0] - wind[0]) / 30;
		}
		windUpdated = true;
	}

	public void produceParticles(double x, double y, int kind) {

		ArrayList<Particle> p = new ArrayList<>();
		if (kind == JMPPARTICLES) {
			for (int i = 0; i != JMPAMOUNT; i++) {
				p.add(new JumpParticle(x, y));
			}

		}

		if (kind == WINDPARTICLES) {

			for (int i = 0; i != WINDAMOUNT; i++) {
				p.add(new WindParticle(x, Math.random() * Settings.HEIGHT));
			}

		}
		particles.addAll(p);

	}

	public int size() {
		return particles.size();
	}

	public void update() {
		int random = (int) Math.floor(Math.random() * 50);
		int changeWind = (int) Math.floor(Math.random() * 30);
		
		if (random == 15) {
			produceParticles(-Camera.scrollX + Settings.GAMEWIDTH,
					Math.random() * Settings.HEIGHT, WINDPARTICLES);
		}

		if (changeWind == 15) {
			changeWind();
		}
		
		if (windUpdated) {
			if (windx != -10) {
				if ((int) wind[0] == (int) newWind[0]) {
					windUpdated = false;
				}
				wind[0] += windx;
			}
			if (windy != -10) {
				if ((int) wind[1] == (int) newWind[1]) {
					windUpdated = false;
				}
				wind[1] += windy;
			}
		}

		if (!particles.isEmpty()) {
			int total = particles.size();
			for (int i = 0; i != total; i++) {
				Particle part = particles.get(i);
				if (!part.update(wind)) {
					particles.remove(i);
					total--;
					i--;
				}
			}
		}
	}

	public synchronized void draw(Graphics2D g) {

		for (Particle part : particles) {

			part.draw(g);

		}
	}
}
