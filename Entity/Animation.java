package Entity;

import java.awt.image.BufferedImage;

public class Animation {

	private BufferedImage[] frames;
	private int currentFrame;
	private int numFrames;
	private int count;
	private int delay;
	private int timesPlayed;
	private double speed;
	private int counter;
	private boolean freezeOnLastFrame = false;
	
	public Animation(double speed) {
		timesPlayed = 0;
		this.speed = speed;
		counter = 0;
	}
	
	public void freezeOnLastFrame(){
		freezeOnLastFrame = true;
	}

	public void setFrames(BufferedImage[] frames) {
		this.frames = frames;
		currentFrame = 0;
		count = 0;
		timesPlayed = 0;
		delay = 2;
		numFrames = frames.length;
	}
	 
	public int getNumPlayed(){
		return this.timesPlayed;
	}

	public void setTimesPlayed(int timesPlayed) {
		this.timesPlayed = timesPlayed;
	}

	public void setDelay(int i) {
		delay = i;
	}

	public void setFrame(int i) {
		currentFrame = i;
	}

	public void setNumFrames(int i) {
		numFrames = i;
	}

	public void reverseUpdate() {
		if (delay == -1)
			return;

		count++;
		if (count == delay) {
			currentFrame--;
			count = 0;
		}
		if (currentFrame <= 0) {
			currentFrame = numFrames - 1;
			timesPlayed++;
		}

	}

	public void update() {
		counter++;
		if(counter%speed==0){
			if (delay == -1)
				return;

			count++;
			if (count == delay) {
				currentFrame++;
				count = 0;
			}
			if (currentFrame == numFrames && !freezeOnLastFrame) {
				currentFrame = 0;
				timesPlayed++;
			} else if(currentFrame == numFrames) {currentFrame = numFrames -1;}
		}
	}

	public int getFrame() {
		return currentFrame;
	}

	public BufferedImage getImage() {
		return frames[currentFrame];
	}


}