package Entity;

public class WindParticle extends Particle{
	public WindParticle (double x, double y){
		super(x, y);
		dy= Math.random()*(1+1)-1;
		dx =  Math.random()*-2;
		ttl =450;
		alpha = 0;
		fade = 0.002;
		fadeinfactor = 0.015;
		WIDTH=2;
		HEIGHT=2;
		rotation = 0;
		rotateSpeed = 0.01;
		hasWind = true;
		fadeIn=true;
	}
	
}
