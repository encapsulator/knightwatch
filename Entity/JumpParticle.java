package Entity;

public class JumpParticle extends Particle {

	public JumpParticle(double x, double y) {
		super(x, y);
		dy= Math.random()*(4+4)-4;
		dx =  Math.random()*(4+4)-4;
		ttl =100;
		alpha = 1;
		fade = 0.01;
		WIDTH=1;
		HEIGHT=1;
		rotation = 0;
		rotateSpeed = 0.01;
		hasWind = false;
		// TODO Auto-generated constructor stub
	}
}
