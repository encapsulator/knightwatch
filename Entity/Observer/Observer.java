package Entity.Observer;
import Entity.MapEntity;

public abstract class Observer {
	public abstract void update(MapEntity p);
}
