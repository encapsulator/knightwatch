package Entity.Observer;
import java.util.Map;

import Entity.Animation;
import Entity.MapEntity;
import Main.Settings;

import static KeyHandler.KeyHandler.*;

public class AttackObserver extends Observer {
	public static int timesPlayed = -1;

	private int counter = Settings.ATTACKRECHARGETIME;
	private MapEntity subject;

	public AttackObserver(MapEntity subject) {
		this.subject = subject;
	}

	@Override
	public void update(MapEntity p) {
		// TODO Auto-generated method stub

		if (subject.equals(p)) {
			counter++;
			Map<String, Animation> animations = p.getAnimations();
			if (p.isAttacking && !p.isDying && !p.isDead && !p.isHurt) {
				if ((!p.inAir && animations.get("attacking").getNumPlayed() < timesPlayed)
						|| (p.inAir && animations.get("jumpAttacking").getNumPlayed() < timesPlayed)) {
					if (!p.inAir) {
						animations.get("attacking").update();
					} else {
						animations.get("jumpAttacking").update();
					}
				} else {
					p.isAttacking = false;
				}
			} else if (isPressed(CTRL) && !p.isDying && !p.isDead && !p.isHurt && !p.isAttacking) {
				if (counter >= Settings.ATTACKRECHARGETIME) {
					counter = 0;
					p.executeCommand("ATTACK");
					if (p.inAir) {
						timesPlayed = animations.get("jumpAttacking").getNumPlayed() + 1;
					} else {
						timesPlayed = animations.get("attacking").getNumPlayed() + 1;
					}
				}
			}

		}
	}
}
