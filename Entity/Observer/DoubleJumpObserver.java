package Entity.Observer;

import Entity.MapEntity;
import Entity.ParticleEngine;
import Shape.Rectangle;

import static KeyHandler.KeyHandler.*;;

public class DoubleJumpObserver extends Observer {
	private MapEntity subject;
	
	public DoubleJumpObserver(MapEntity subject){
		this.subject = subject;
	}
	@Override
	public void update(MapEntity p) {
		if(p.equals(subject)){
		// TODO Auto-generated method stub
		if (!isPressed(UP) && p.inAir) {
			p.dubbleJump = false;
		}
		// double Jump
		if (p.jumpsLeft > 0 && p.inAir && isPressed(UP) && !p.dubbleJump) {
			ParticleEngine.getInstance().produceParticles(
					p.getShape().position.x + ((Rectangle)p.getShape()).width / 2,
					((Rectangle)p.getShape()).height + p.getShape().position.y - 30,
					ParticleEngine.getInstance().JMPPARTICLES);
			p.executeCommand("dubbleJump");
		}
	}
	}
}
