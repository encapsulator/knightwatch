package Entity.Observer;

import java.util.Map;

import Entity.Animation;
import Entity.MapEntity;

import static KeyHandler.KeyHandler.*;

public class CrouchObserver extends Observer {

	private MapEntity subject;
	
	public CrouchObserver(MapEntity subject){
		this.subject = subject;
	}
	
	@Override
	public void update(MapEntity p) {
		if(p.equals(subject)){
			Map<String, Animation> animations = p.getAnimations();
		if(p.isCrouchWalking && !p.isDying && !p.isDead){
			p.updateAnimation(animations.get("croucheWalking"));
		}
		// TODO Auto-generated method stub
		if(p.isCrouching){
			if(isPressed(LEFT) || isPressed(RIGHT)){
				p.executeCommand("crouchWalk");
			}
		}
		if(!p.inAir && isPressed(DOWN)){
			p.executeCommand("crouch");
		}else if(!isPressed(DOWN)){
			p.isCrouchWalking = false;
			p.isCrouching = false;
		}
		if(isPressed(DOWN) && !isPressed(LEFT) && !isPressed(RIGHT)){
			p.isCrouchWalking = false;
			p.isCrouching = true;
		}
	}
	}
}
