package Entity.Observer;
import java.util.Map;

import Entity.Animation;
import Entity.MapEntity;

import static KeyHandler.KeyHandler.*;;

public class InAirObserver extends Observer {
	
	private MapEntity subject;
	public InAirObserver(MapEntity subject){
		this.subject = subject;
	}
	@Override
	public void update(MapEntity p) {
		// TODO Auto-generated method stub
		if(p.equals(subject)){
			if (p.inAir) {

				if (isPressed(LEFT) || isPressed(RIGHT)) {
					Map<String, Animation> animations = p.getAnimations();
					Animation jumping = animations.get("jumping");
					if(jumping!=null)p.updateAnimation(jumping);
				}

			}
			p.executeCommand("gravity");
		}
	}
}
