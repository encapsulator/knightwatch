package Entity.Observer;

import Entity.MapEntity;

import static KeyHandler.KeyHandler.*;;

public class JumpObserver extends Observer {

	private MapEntity subject;

	public JumpObserver(MapEntity subject) {
		this.subject = subject;
	}

	@Override
	public void update(MapEntity p) {
		if (p.equals(subject)) {
			// TODO Auto-generated method stub

			if (isPressed(UP) && !p.inAir && !p.isDying && !p.isDead && !p.isHurt) {
				p.executeCommand("JUMP");
			}
		}
	}

}
