package Entity.Observer;

import Entity.MapEntity;

import static KeyHandler.KeyHandler.*;;

public class FlyObserver extends Observer {
	
	private MapEntity subject;
	public FlyObserver(MapEntity subject){
		this.subject = subject;
	}

	@Override
	public void update(MapEntity p) {
		// TODO Auto-generated method stub
		if(subject.equals(p)){
		if(isPressed(SPACE)){
			p.executeCommand("fly");
		}
		}
	}
}
