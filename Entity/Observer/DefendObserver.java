package Entity.Observer;

import java.util.Map;

import Entity.Animation;
import Entity.MapEntity;


import static KeyHandler.KeyHandler.*;

public class DefendObserver extends Observer {
	private MapEntity subject;
	
	public DefendObserver(MapEntity subject){
		this.subject = subject;
	}
	
	@Override
	public void update(MapEntity p) {
		
		if(p.equals(subject)){
			Map<String, Animation> animations = p.getAnimations();
		// TODO Auto-generated method stub
		if(!isPressed(ALT)){
			p.isDefending = false;
		}else{
			if(!p.isDefending){
				p.paralize=true;
				p.executeCommand("defend");
			}else{
				p.executeCommand("slowDown");
				if(animations.get("defending").getFrame()<=1){
					animations.get("defending").update();
				}
			}
		}
		}
	}
	
}
