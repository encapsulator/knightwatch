package Entity.Observer;

import Entity.Animation;
import Entity.MapEntity;
import GameState.LevelState;
import Main.Background;
import Main.Camera;

import java.util.Map;

import static KeyHandler.KeyHandler.*;;

public class XMovementObserver extends Observer {
	private MapEntity subject;
	
	public XMovementObserver(MapEntity subject){
		this.subject = subject;
	}
	@Override
	public void update(MapEntity p) {
		if(p.equals(subject)){
		// TODO Auto-generated method stub
			if(p.getShape().position.x<50){
				p.direction.x = 0;
				p.getShape().position.x = 50;
			}
				Map<String, Animation> animations = p.getAnimations();

			if(!p.paralize){

				if (isPressed(LEFT) && !p.isDying && !p.isDead && !p.isHurt) { // left or right
					animations.get("walking").update();
					p.executeCommand("moveLeft");
				} else if (isPressed(RIGHT) && !p.isDying && !p.isDead && !p.isHurt) {
					Background.setHasStarted(true);
					Camera.resetScrollSpeed();
					animations.get("running").update();
					p.executeCommand("moveRight");
				} else {
					p.executeCommand("slowDown");
				}
			} else {

				if (!Camera.scrollBack && !p.inAir) {
					p.paralize = false;
					LevelState.reloading=false;
				}
			}
		}
	}
}
