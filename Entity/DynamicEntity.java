package Entity;

import Main.Updateable;
import Math.Vector;
/**
 * Created by niels on 20/04/2017.
 */
public abstract class DynamicEntity extends Entity implements Updateable {
    public Vector direction = new Vector(0,0);
}
