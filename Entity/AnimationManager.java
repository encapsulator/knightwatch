package Entity;

import Main.ImageLoader;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

public class AnimationManager {
	public AnimationManager(){}

	private HashMap<String, Animation> animations = new HashMap<>();
	
	public void registerAnimation(String str, String animationImage, int speed, int length, boolean freezeOnLastFrame) throws IOException {
		BufferedImage animation = ImageIO.read(getClass().getResourceAsStream(animationImage));
		animation = ImageLoader.getInstance().loadImage(animation);

		Animation anim = new Animation(speed);
		anim = loadSprites(animation, anim, length);
		if(freezeOnLastFrame) anim.freezeOnLastFrame();
		getAnimations().put(str, anim);
	}

	public HashMap<String, Animation> getAnimations() {
		return animations;
	}

	public Animation getAnimation(String key) {
		return animations.get(key);
	}

	private Animation loadSprites(BufferedImage image, Animation a, int length){
		BufferedImage[] sprites = new BufferedImage[length];
		for(int i=0;i<image.getHeight()/128;i++){
			for(int j=0;j<image.getWidth()/128;j++){
				if(i+j<length){
					BufferedImage newimage = image.getSubimage(j*128, i*128, 128, 128);
					sprites[i * (image.getWidth() / 128) + j] = newimage;
				}
			}
		}
		a.setFrames(sprites);
		return a;
	}

	public void setAnimations(HashMap<String, Animation> animations) {
		this.animations = animations;
	}
}
