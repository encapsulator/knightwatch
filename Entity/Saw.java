package Entity;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import Main.ImageLoader;
import Main.Settings;
import Shape.Circle;
import TileMap.TileMap;

public class Saw extends MapEntity {
	private double x,y;

	private BufferedImage image;
	public int rotationTime = Settings.ROTATIONTIME;
	public double angle;
	
	public Saw(double x, double y, TileMap t)  {
		super(t);
		this.x = x;
		this.y = y;
		try {
			image = ImageIO.read(getClass()
                    .getResourceAsStream(Settings.SAW));
		} catch (IOException e) {
			e.printStackTrace();
		}
		image = ImageLoader.getInstance().loadImage(image);
		shape = new Circle(this.x+64, this.y+64, 51);
		
	}
	@Override
	public void draw(Graphics2D g) {
		// TODO Auto-generated method stub

		AffineTransform at = new AffineTransform();
		at.translate(x+image.getWidth()/2,y+image.getHeight()/2);
		at.rotate(angle*(Math.PI/180));
		at.translate(-image.getWidth()/2, -image.getHeight()/2);
		g.drawImage(image, at,null);

	}


	@Override
	public void update() {
		rotationTime--;
		if(rotationTime==0) {
			rotationTime = Settings.ROTATIONTIME;
			angle += Settings.ROTATIONPERTIME;
		}
		angle=angle==90?0:angle;
	}
}
