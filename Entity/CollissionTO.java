package Entity;

public class CollissionTO {
	public Double x, y, dx, dy;
	public Boolean collides;
	public boolean stopFurtherCollission, inAir;
	public CollissionTO(){}
	public CollissionTO(Double x, Double y, Double dx, Double dy,
						boolean furtherCol, boolean inAir) {
		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
		this.stopFurtherCollission = furtherCol;
		this.inAir=inAir;
	}

}
