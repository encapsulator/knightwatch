package Entity;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import Main.Camera;
import Main.GamePanel;
import Main.Settings;

public abstract class Particle{

	protected double WIDTH;
	protected double HEIGHT;
	protected double[] wind;
	protected double x, y;
	protected double dx;
	protected double dy;
	protected double rotateSpeed, rotation;
	protected int ttl;
	protected float alpha;
	protected double fade;
	protected boolean hasWind, fadeIn = false;;
	protected double fadeinfactor;

	public Particle(double x, double y) {
		this.x = x;
		this.y = y;

	}

	public boolean update(double[] wind) {
		ttl--;
		x += dx;
		y += dy;
		if (x < -Camera.scrollX - x
				|| x > -Camera.scrollX + Settings.GAMEWIDTH) {
			ttl = 0;
		}
		if (y + WIDTH < 0 || y > Settings.HEIGHT) {
			ttl = 0;
		}
		if (hasWind) {

			x += wind[0];
			y += wind[1];
		}
		rotation += rotateSpeed;
		if (fadeIn) {
			alpha += fadeinfactor;
			if (alpha >= 1) {
				fadeIn = false;
			}
		} else {
			alpha -= fade;
			alpha = alpha < 0 ? 0 : alpha;
		}
		if (alpha >= 1)
			alpha = 1;

		if (ttl == 0)
			return false;
		return true;
	}

	public void draw(Graphics2D g) {
		Graphics2D gr = (Graphics2D) g.create();
		gr.rotate(rotation, x + WIDTH / 2, y + HEIGHT / 2);
		gr.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
				alpha));
		Color c = new Color(255, 255, 255);
		gr.setPaint(c);
		gr.fillRect((int) x, (int) y, (int) WIDTH, (int) HEIGHT);
	}

}
