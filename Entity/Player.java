/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entity;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;
import Entity.Command.*;
import Entity.Observer.*;
import Main.*;
import Shape.Rectangle;
import Shape.Shape;
import TileMap.CoinMap;
import TileMap.TileMap;
import Math.Vector;

import static KeyHandler.KeyHandler.*;

/**
 * @author niels
 */
public class Player extends MapEntity {

    private int health = Settings.HEALTH;
    private CompositeEntity enemies;
    private int hurtdelay, resetDelay, getAliveDelay;

    public Player(double x, double y, TileMap t) {
        super(t);
        shape = new Rectangle(x, y, Settings.PLAYERSHAPEWIDTH, Settings.PLAYERSHAPEHEIGHT);
        animationManager = new AnimationManager();
        try {
            animationManager.registerAnimation("dying", Settings.PLAYER_DIE, 2, 5, true);
            animationManager.registerAnimation("hurt", Settings.PLAYER_HURT, 2, 4, false);
            animationManager.registerAnimation("idle", Settings.PLAYER_IDLE, 1, 5, false);
            animationManager.registerAnimation("jumping", Settings.PLAYER_JUMP, 1, 1, false);
            animationManager.registerAnimation("walking", Settings.PLAYER_WALK, 1, 12, false);
            animationManager.registerAnimation("attacking", Settings.PLAYER_ATTACK, 1, 9, false);
            animationManager.registerAnimation("defending", Settings.PLAYER_DEFEND, 1, 3, false);
            animationManager.registerAnimation("jumpAttacking", Settings.PLAYER_JUMP_ATTACK, 1, 6, false);
            animationManager.registerAnimation("running", Settings.PLAYER_RUN, 1, 14, false);
            animationManager.registerAnimation("crouching", Settings.PLAYER_CROUCH, 1, 1, false);
            animationManager.registerAnimation("croucheWalking", Settings.PLAYER_CROUCH_WALK, 2, 6, false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        addCommand("JUMP", new Jump());
        addCommand("dubbleJump", new DubbleJump());
        addCommand("fly", new Fly());
        addCommand("slowDown", new SlowDown());
        addCommand("gravity", new ApplyGravity());
        addCommand("ATTACK", new Attack());
        addCommand("defend", new Defend());
        addCommand("crouch", new Crouch());
        addCommand("crouchWalk", new CrouchWalk());
        addCommand("moveLeft", new MoveLeft());
        addCommand("moveRight", new MoveRight());
        attach(new DoubleJumpObserver(this));
        attach(new InAirObserver(this));
        attach(new JumpObserver(this));
        attach(new FlyObserver(this));
        attach(new AttackObserver(this));
        attach(new XMovementObserver(this));
        attach(new DefendObserver(this));
        attach(new CrouchObserver(this));
        load();
    }

    public void setEnemies(CompositeEntity enemies) {
        this.enemies = enemies;
        for (MapEntity me : enemies.getEntities()) {
            if (me instanceof SkeletonEnemy) {
                ((SkeletonEnemy) me).setPlayer(this);
            }
        }
    }


    public void load() {
        shape.position.x = 50;
        shape.position.y=50;
        direction.x = direction.y = 0;
        dubbleJump = inAir = true;
        health = Settings.HEALTH;
        isDead = false;
        isHurt = false;
        isDying = false;
        paralize = true;
        inAir = true;
        hurtdelay = Settings.HURTDELAY;
        resetDelay = Settings.RESETAFTERDEADDELAY;
        getAliveDelay = Settings.GETALIVEDELAY;

    }

    public HashMap<String, Animation> getAnimations() {
        return animationManager.getAnimations();
    }

    public AnimationManager getAnimationManager() {
        return animationManager;
    }

    @Override
    public void draw(Graphics2D g) {
        enemies.draw(g);
        super.draw(g);
        CoinMap.draw(g);

        if (isHurt || ((isDying || isDead) && inAir))
            g.drawImage(animationManager.getAnimations().get("hurt").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);
        else if (isDying || isDead)
            g.drawImage(animationManager.getAnimations().get("dying").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);
        else if (inAir && isAttacking)
            g.drawImage(animationManager.getAnimations().get("jumpAttacking").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);
        else if (inAir)
            g.drawImage(animationManager.getAnimations().get("jumping").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);
        else if (isAttacking)
            g.drawImage(animationManager.getAnimations().get("attacking").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);
        else if (isDefending)
            g.drawImage(animationManager.getAnimations().get("defending").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);
        else if (isCrouchWalking)
            g.drawImage(animationManager.getAnimations().get("croucheWalking").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);
        else if (isCrouching)
            g.drawImage(animationManager.getAnimations().get("crouching").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);
        else if (isPressed(LEFT))
            g.drawImage(animationManager.getAnimations().get("walking").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);
        else if (isPressed(RIGHT))
            g.drawImage(animationManager.getAnimations().get("running").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);
        else
            g.drawImage(animationManager.getAnimations().get("idle").getImage(), (int) shape.position.x - 40, (int) shape.position.y - 28, null);

    }

    public void hit(double damage) {
        health -= damage;
        LevelUI.getInstance().setHealth(health);
        isHurt = true;
        hurtdelay = Settings.HURTDELAY;
        direction.x -= Settings.PLAYERGOTHITPUSHBACK;
        if (health <= 0) {
            isDying = true;
            paralize = true;
            isHurt = false;
            resetDelay = Settings.RESETAFTERDEADDELAY;
        }
    }

    @Override
    public void update() {

        // able to dubblejump when jumpkey released in air
        notifyObservers();
        enemies.update();

        for (MapEntity me : enemies.getSurroundingEntities(shape.position.add(direction).x, shape.position.add(direction).y).stream().filter(x -> (x instanceof Saw)).collect(Collectors.toSet())) {
            Saw saw = (Saw) me;
            if (shape.accept(saw.shape, direction).collides) {
                isDying = true;
                resetDelay = Settings.RESETAFTERDEADDELAY;
                jumpsLeft = 0;
            }
        }

        if (shape.position.x + ((Rectangle) shape).width < -Camera.scrollX && !isDead && !Camera.scrollBack) {
            isDead = true;
            resetDelay = Settings.RESETAFTERDEADDELAY;
        }
        if (shape.position.y + direction.y >= Settings.HEIGHT) {
            isDying = false;
            isDead = true;
            resetDelay = Settings.RESETAFTERDEADDELAY;
        }
        if (isHurt) hurtdelay--;
        if (hurtdelay <= 0) {
            isHurt = false;
            hurtdelay = Settings.HURTDELAY;
        }
        if (isDying && !isDead && !inAir) {

            animationManager.getAnimations().get("dying").update();
            this.resetDelay--;
            if (resetDelay <= 0) {
                isDying = false;
                isDead = true;
                resetDelay = Settings.RESETAFTERDEADDELAY;
            }
        }
        if (isDead) {
            getAliveDelay--;
            if (getAliveDelay <= 0) {
                isDead = false;
                getAliveDelay = Settings.GETALIVEDELAY;
            }

        }
        checkCollission();
        checkCoinCollission();
        shape.translate(new Vector(direction.x, direction.y));
        Background.getInstance().setSpeed(direction.x);
    }

    public void checkCoinCollission() {
        int row = (int) shape.position.y / Settings.COINTILESIZE;
        int col = (int) shape.position.x / Settings.COINTILESIZE;

        ArrayList<Coin> coinsList = CoinMap.getCloseCoins(row, col);

        for (int i = 0; i != coinsList.size(); i++) {
            Coin e = coinsList.get(i);
            if (e.isVisible()) {
                Shape coinShape = e.getShape();

                boolean overlap = shape.overlaps(coinShape);
                if (overlap) {
                    LevelUI levelUI = LevelUI.getInstance();
                    levelUI.setAmountCoins(levelUI.getAmountCoins()+Settings.COINVALUE);
                    CoinMap.removeCoin(e);
                    coinsList.remove(i);
                    try {
                        DiskJockey.playClip(DiskJockey.COIN, false);
                    } catch (Exception e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                    i--;

                }
            }
        }
    }

    public void checkCollission() {
        boolean res = super.checkTileMapCollission();
        if (res) {
            if (!isAttacking) {
                inAir = dubbleJump = false;
                onFloor = true;
                jumpsLeft = Settings.TOTALJUMPS;
            } else if (isAttacking && inAir) {
                inAir = dubbleJump = false;
                onFloor = true;
                jumpsLeft = Settings.TOTALJUMPS;
                Animation jumpAttacking = getAnimations().get("jumpAttacking");
                int attackFrame = jumpAttacking.getFrame();
                jumpAttacking.setTimesPlayed(jumpAttacking.getNumPlayed());
                jumpAttacking.setFrame(0);
                Animation attacking = getAnimations().get("attacking");
                AttackObserver.timesPlayed = attacking.getNumPlayed() + 1;
                attacking.setFrame(attackFrame + 3);
            }
        } else {
            inAir = true;
            if(onFloor) {
                onFloor=false;
                jumpsLeft-=1;
            }
        }
    }
}
