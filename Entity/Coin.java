package Entity;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import Main.Camera;
import Main.ImageLoader;
import Main.Settings;
import Shape.Rectangle;

public class Coin extends Entity{

	private BufferedImage image;
	AnimationManager animationManager;

	public Coin(double x, double y) throws IOException {
		animationManager = new AnimationManager();
		animationManager.registerAnimation("rotate",Settings.COIN, 2, 9, false);
		image = ImageIO.read(getClass().getResourceAsStream(Settings.COIN));
		shape = new Rectangle(x, y, Settings.COINTILESIZE, Settings.COINTILESIZE);
		init();
	}
	
	

	public void init() {
		BufferedImage[] images = new BufferedImage[(int) ((image.getHeight() / Settings.COINTILESIZE) * (image
				.getWidth() / Settings.COINTILESIZE))];
		int count = 0;
		for (int row = 0; row != image.getHeight() / Settings.COINTILESIZE; row++) {
			for (int col = 0; col != image.getWidth() / Settings.COINTILESIZE; col++) {
				BufferedImage subImage = image.getSubimage(col * Settings.COINTILESIZE, row
						* Settings.COINTILESIZE, Settings.COINTILESIZE, Settings.COINTILESIZE);
				subImage = ImageLoader.getInstance().loadImage(subImage);
				images[count] = subImage;
				count++;
			}
		}
		animationManager.getAnimations().get("rotate").setFrames(images);
	}

	public void update() {
		if (isVisible()) {
			animationManager.getAnimations().get("rotate").update();
		}
	}

	public boolean isVisible() {
		int gameWidth = Settings.WIDTH;
		double scrolled = Camera.scrollX;
		return shape.position.x > scrolled && shape.position.x < gameWidth;
	}

	public void draw(Graphics2D g) {
		if (isVisible()) {
			g.drawImage(animationManager.getAnimations().get("rotate").getImage(), (int) shape.position.x, (int) shape.position.y, null);
		}
	}

}
