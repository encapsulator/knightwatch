/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Shape;

import Math.Vector;
import Shape.collision.ICollidable;
import Shape.collision.IVisitor;

/**
 *
 * @author niels
 */
public abstract class Shape implements ICollidable, IVisitor{

    public Vector position;
    
    public Shape(double x, double y){
        position = new Vector(x, y);
    }
    
    public void translate(Vector v){
        position = position.add(v);
    }
    
    public boolean overlaps(Shape s){
    	if(this instanceof Rectangle && s instanceof Rectangle){
    		Rectangle a = (Rectangle)this;
    		Rectangle b = (Rectangle)s;
    		return a.intersects(b);
    	}
    	return false;
    }




}
