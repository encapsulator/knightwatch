package Shape.collision;

import Entity.CollissionTO;
import Math.Vector;
/**
 * Created by niels on 21/04/2017.
 */
public interface ICollidable {
    CollissionTO accept(IVisitor other, Vector direction);
}
