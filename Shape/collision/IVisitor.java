package Shape.collision;

import Entity.CollissionTO;
import Shape.Circle;
import Shape.Rectangle;
import Shape.Triangle;
import Math.Vector;
/**
 * Created by niels on 21/04/2017.
 */
public interface IVisitor {
    CollissionTO collides(Rectangle r, Vector direction);
    CollissionTO collides(Circle c, Vector direction);
    CollissionTO collides(Triangle t, Vector direction);
}
