package Shape;

import java.awt.Color;
import java.awt.Graphics2D;

import Entity.CollissionTO;
import Math.Vector;
import Shape.collision.IVisitor;

public class Triangle extends Shape {
	 protected boolean up;
	 protected double width, height;
	 
	 public Triangle(double x, double y, double width, double height, boolean up){
	        super(x, y);
	        this.width = width;
	        this.height = height;
	        this.up=up;
	 }
	 
	 public boolean goesUp(){return up;}
	 
	 public void draw(Graphics2D g){
		 double[][] border = getBorder();
		 int[] xP, yP;
		 xP=new int[3];
		 yP=new int[3];
		 for(int i=0;i!=3;i++){
			 xP[i] = (int)border[i][1];
			 yP[i] = (int)border[i][0];
		 }
		 
		 g.setColor(Color.BLUE);
		 g.drawLine(xP[0], yP[0], xP[1], yP[1]);
		 g.setColor(Color.BLUE);
		 g.drawLine(xP[0], yP[0], xP[2], yP[2]);
		 g.setColor(Color.BLUE);
		 g.drawLine(xP[1], yP[1], xP[2], yP[2]);
	 }
	 
	 public double[][] getBorder(){
		 double[][] res = new double[3][2];
		 if(!up){
		 	res[0][0] = position.y;
		 	res[0][1] = position.x;
		 	res[1][0] = res[0][0] + height;
		 	res[1][1] = position.x;
		 	res[2][0] = res[1][0];
		 	res[2][1] = res[0][1] + width;
	 	}else {
			 res[0][0] = position.y;
		 	 res[0][1] = position.x+width;
		 	 res[1][0] = position.y+height;
		 	 res[1][1] = res[0][1];
		 	 res[2][0] = res[1][0];
		 	 res[2][1] = position.x;
		 }
		 return res;
	 }

	 public Vector[] getPoints(){
	 	double[][] border = getBorder();
	 	Vector[] vectors = new Vector[3];
	 	vectors[0] = new Vector((int)border[0][1], (int)border[0][0]);
	 	vectors[1] = new Vector((int)border[1][1], (int)border[1][0]);
		vectors[2] = new Vector((int)border[2][1], (int)border[2][01]);
		return vectors;
	 }

	public Border getBBorder() {
		// TODO Auto-generated method stub
		Border b = new Border();
		b.left = position.x;
		b.right = position.x+width;
		b.up = position.y;
		b.down =  position.y + height;
		b.slopeUp = up;
		return b;
	}


	@Override
	public CollissionTO accept(IVisitor other, Vector direction) {
		return other.collidesWithTriangle(this,direction);
	}

	@Override
	public CollissionTO collidesWithRectangle(Rectangle r, Vector direction) {
		return r.collidesWithTriangle(this,direction);
	}

	@Override
	public CollissionTO collidesWithCircle(Circle c, Vector direction) {
		return c.collidesWithTriangle(this,direction);
	}

	@Override
	public CollissionTO collidesWithTriangle(Triangle t, Vector direction) {
		CollissionTO res = new CollissionTO();

		//An edge from first triangle collides with edge second triangle
		Vector[] t1 = getPoints();
		Vector[] t2 = t.getPoints();

		for (int i = 0; i != t1.length; ++i) {

			int prevPointT1 = i - 1 < 0 ? t1.length - 1 : i - 1;
			Vector t1e = t1[i].substract(t1[prevPointT1]);

			for (int j = 0; j != t2.length; ++j) {

				int prevPointT2 = j - 1 < 0 ? t2.length - 1 : j - 1;
				Vector t2e = t2[j].substract(t2[prevPointT2]);

				//t1e1 x t2e1
				Vector subp3p1 = t2[prevPointT2].substract(t1[prevPointT1]);
				double denominator = t1e.cross(t2e);

				if (denominator != 0) {
					double nominator1 = t2e.cross(subp3p1);
					double nominator2 = t1e.cross(subp3p1);
					double ua = nominator1 / denominator;
					double ub = nominator2 / denominator;

					if (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1) {
						res.collides = true;
						return res;
					}

				}

			}
		}

		boolean t1p1int2 = pointInTriangle(t1[0],t2[0], t2[1], t2[2]);
		boolean t1p2int2 = pointInTriangle(t1[1],t2[0], t2[1], t2[2]);
		boolean t1p3int2 = pointInTriangle(t1[2],t2[0], t2[1], t2[2]);
		boolean t2p1int1 = pointInTriangle(t2[0],t1[0], t1[1], t1[2]);
		boolean t2p2int1 = pointInTriangle(t2[1],t1[0], t1[1], t1[2]);
		boolean t2p3int1 = pointInTriangle(t2[2],t1[0], t1[1], t1[2]);

		res.collides = t1p1int2 || t1p2int2 || t1p3int2 || t2p1int1 || t2p2int1 || t2p3int1;
		return res;
	}

	private boolean sameSide(Vector p1,Vector p2, Vector a,Vector b) {
		double cp1 = b.substract(a).cross(p1.substract(a));
		double cp2 = b.substract(a).cross(p2.substract(a));
		return cp1*cp2 >= 0;
	}

	private boolean pointInTriangle(Vector p,Vector a, Vector b,Vector c) {
		return sameSide(p,a, b,c) && sameSide(p,b, a,c)
		&& sameSide(p,c, a,b);
	}


}
