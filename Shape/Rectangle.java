/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Shape;

import Entity.CollissionTO;
import Math.Vector;
import Shape.collision.IVisitor;
import TileMap.Tile;
import TileMap.TileMap;
/**
 *
 * @author niels
 */

public class Rectangle extends Shape {

	public double width, height;

	public Rectangle(double x, double y, double width, double height) {
		super(x, y);
		this.width = width;
		this.height = height;
	}

	public double[] calculateHitPointTr(Vector direction, double rowP, double colP, Border self, Border shape){
		double[] res = new double[2];
		if(!shape.slopeUp){
			while (direction.x < 0 && direction.y > 3 && self.down > shape.down - rowP) {
				direction.x /= 2;direction.y /= 2;
				colP = self.right + direction.x - (width * 3) / 4;
				rowP = shape.right - colP;
			}
		}else{
			while (direction.x > 0 && direction.y > 3 && self.down > shape.down - rowP) {
				direction.x /= 2;direction.y /= 2;
				colP = self.right + direction.x - width / 4;
				rowP = colP-shape.left;
			}
		}
		res[0]=colP;
		res[1]=rowP;
		return res;
	}

	public Border getBorder(){
		Border b = new Border();
		b.left = position.x;
		b.right = position.x+width;
		b.up = position.y;
		b.down = position.y+height;
		return b;
	}

	public boolean intersects(Rectangle b) {

		return b.width > 0 && b.height > 0 && width > 0 && height > 0
				&& b.position.x < position.x + width && b.position.x + b.width > b.position.x
				&& b.position.y < position.y + height && b.position.y + b.height > position.y;
	}


	@Override
	public CollissionTO accept(IVisitor other, Vector direction) {
		return other.collidesWithRectangle(this, direction);
	}

	@Override
	public CollissionTO collidesWithRectangle(Rectangle r, Vector direction) {
		Border self = getBorder();
		Border shape = r.getBorder();
		CollissionTO res = null;
		boolean vertical = self.right > shape.left && self.left < shape.right;
		boolean horizontal = self.down > shape.up && self.up < shape.down;

		if (vertical) {
			if (self.down <= shape.up && self.down + direction.y > shape.up) {//up
				res = new CollissionTO(null, shape.up - height, null, 0.0, false, false);
			}
			else if (self.up >= shape.down && self.up + direction.y < shape.down) {//down
				res = new CollissionTO(null,shape.down, null, 0.0,false, true);
			}
		} else if (horizontal) {
			if (self.right <= shape.left && self.right + direction.x > shape.left) {//left
				res = new CollissionTO(shape.left - width -1, null, 0.0, null, false, false);
			}
			else if (self.left >= shape.right && self.left + direction.x < shape.right) {//right
				res = new CollissionTO(shape.right+1, null, 0.0, null, false, false);
			}
		}
		return res;
	}

	@Override
	public CollissionTO collidesWithCircle(Circle c, Vector direction) {
		return c.collidesWithRectangle(this, direction);
	}

	@Override
	public CollissionTO collidesWithTriangle(Triangle s, Vector direction) {
		Border self = getBorder();
		Border shape = s.getBBorder();

		boolean horizontal = self.down > shape.up && self.up < shape.down;
		boolean vertical = self.right > shape.left && self.left < shape.right;
		int row = (int) s.position.y / 64, col =(int)s.position.x/64;

		// point of collission
		double colP = shape.slopeUp?self.right + direction.x - width / 4 : self.right + direction.x - (width * 3) / 4;
		double rowP = shape.slopeUp?(colP) - shape.left : shape.right-colP;// levitation
		int OPPOSITE = shape.slopeUp?Tile.SLOPEDOWN:Tile.SLOPEUP;
		int NORMAL = shape.slopeUp?Tile.SLOPEUP:Tile.SLOPEDOWN;
		int colDif = shape.slopeUp?col+1:col-1;

		if (direction.y >= -4 && direction.y <= 2.95) {//horizontal
			if (self.up < shape.down && self.down  >= shape.up) {
				if (colP >= shape.left +(shape.slopeUp?0:direction.x) && colP <= shape.right+(shape.slopeUp?direction.x:0)) {
					boolean stopCol;
					if ((direction.x < 0 && !shape.slopeUp)||( direction.x>0 && shape.slopeUp)) {
						stopCol = (Tile.BLOCKED == TileMap.getSType(row, colDif) || OPPOSITE == TileMap.getSType(row, colDif))&& Tile.BLOCKED != TileMap.getSType(row - 1, colDif);
						return new CollissionTO(null,shape.down-rowP-height, null,0.0, stopCol, false);
					} else {
						int colDif2 = shape.slopeUp?col-1:col+1;

						stopCol = NORMAL == TileMap.getSType(row + 1, colDif2);
						return new CollissionTO(null,shape.down-rowP-height, null,0.0, stopCol, false);
					}
				}
			}
		}

		if(vertical){//down
			if(self.up>=shape.down && self.up+direction.y<shape.down){
				return new CollissionTO(null, shape.down, null,0.0,false,true);
			}
		}
		if (colP <= shape.right && colP > shape.left) {//up
			double[] hitPoint = calculateHitPointTr(direction, rowP, colP, self, shape);
			colP = hitPoint[0]; rowP = hitPoint[1];

			if (self.down <= shape.down - rowP	&& self.down + direction.y > shape.down - rowP) {
				return new CollissionTO(null, shape.down - rowP - height, null, 0.0, true, false);
			}
		}

		if(horizontal && shape.slopeUp){
			if (self.left >= shape.right && self.left + direction.x < shape.right && TileMap.getSType(row, col+1)!=OPPOSITE ) {//right
				return new CollissionTO(shape.right, null, 0.0,0.0, false, false);
			}
		}else if(horizontal){
			if (self.right <= shape.left && self.right + direction.x > shape.left && TileMap.getSType(row , col-1)!=OPPOSITE) {//left
				return new CollissionTO(shape.left - width,null, 0.0, 0.0, false, false);
			}
		}

		if (self.down <= shape.up && self.down + direction.y > shape.up) {//Stay on top of sharp edges
			if ((shape.slopeUp && colP > shape.right && self.left < shape.right) ||
					!shape.slopeUp && colP < shape.left && self.right > shape.left) {
				boolean furtherCol = Tile.BLOCKED != TileMap.getSType(row, colDif);
				return new CollissionTO(null, shape.up - height, null, 0.0, furtherCol, false);
			}
		}
		return null;
	}
}