package Shape;

import Entity.CollissionTO;
import Shape.collision.IVisitor;
import Math.Vector;

/**
 * Created by niels on 14/04/2017.
 */
public class Circle extends Shape {
    public double radius;

    public Circle(double x, double y, double radius) {
        super(x, y);
        this.radius = radius;
    }

    @Override
    public CollissionTO accept(IVisitor other, Vector direction) {
        return other.collidesWithCircle(this, direction);
    }

    @Override
    public CollissionTO collidesWithRectangle(Rectangle r, Vector direction) {
        CollissionTO res = new CollissionTO();
        double circleDistanceX = Math.abs(this.position.x - (r.position.x + r.width / 2));
        double circleDistanceY = Math.abs(position.y - (r.position.y + r.height / 2));

        if (circleDistanceX > (r.width / 2 + radius)) {
            res.collides = false;
        } else if (circleDistanceY > (r.height / 2 + radius)) {
            res.collides = false;
        } else if (circleDistanceX <= (r.width / 2)) {
            res.collides = true;
        } else if (circleDistanceY <= (r.height / 2)) {
            res.collides = true;
        } else {
            double cornerDistance_sq = Math.pow(circleDistanceX - r.width / 2, 2) +
                    Math.pow(circleDistanceY - r.height / 2, 2);
            res.collides = (cornerDistance_sq <= (Math.pow(radius, 2)));
        }
        return res;
    }

    @Override
    public CollissionTO collidesWithCircle(Circle c, Vector direction) {
        Vector circlePos = c.position.substract(position.add(direction));
        double radii = radius + c.radius;
        CollissionTO res = new CollissionTO();
        res.collides = circlePos.cross(circlePos) < radii * radii;
        return res;
    }

    @Override
    public CollissionTO collidesWithTriangle(Triangle t, Vector direction) {

        CollissionTO res = new CollissionTO();

        int v1x = (int) t.getPoints()[0].x;
        int v1y = (int) t.getPoints()[0].y;

        int v2x = (int) t.getPoints()[1].x;
        int v2y = (int) t.getPoints()[1].y;

        int v3x = (int) t.getPoints()[2].x;
        int v3y = (int) t.getPoints()[2].y;

        double centrex = position.x;
        double centrey = position.y;

        double c1x = centrex - v1x;
        double c1y = centrey - v1y;

        double radiusSqr = radius * radius;
        double c1sqr = c1x * c1x + c1y * c1y - radiusSqr;

        if (c1sqr <= 0) {
            res.collides = true;
            return res;
        }

        double c2x = centrex - v2x;
        double c2y = centrey - v2y;
        double c2sqr = c2x * c2x + c2y * c2y - radiusSqr;

        if (c2sqr <= 0) {
            res.collides = true;
            return res;
        }

        double c3x = centrex - v3x;
        double c3y = centrey - v3y;

        double c3sqr = radiusSqr;
        c3sqr = c3x * c3x + c3y * c3y - radiusSqr;

        if (c3sqr <= 0) {
            res.collides = true;
            return res;
        }
        /*;
        TEST 2:Circle centre within triangle;

        ;
        ;
        Calculate edges;*/
        double e1x = v2x - v1x;
        double e1y = v2y - v1y;

        double e2x = v3x - v2x;
        double e2y = v3y - v2y;

        double e3x = v1x - v3x;
        double e3y = v1y - v3y;

        if((e1y * c1x - e1x * c1y > 0) | (e2y * c2x - e2x * c2y > 0) | (e3y * c3x - e3x * c3y > 0)){
            res.collides = true;
            return res;
        }

        /*TEST 3:Circle intersects edge
        ;*/
        double k = c1x * e1x + c1y * e1y;

        if (k > 0)
        {
            double len = e1x * e1x + e1y * e1y;
            len = Math.sqrt(len);

            if (k<len)
            {
                if(c1sqr * len <= k * k){
                    res.collides = true;
                    return res;
                }
            }
        }

        /*;
        Second edge*/
        k = c2x * e2x + c2y * e2y;

        if (k > 0)
        {
            double len = e2x * e2x + e2y * e2y;
            len = Math.sqrt(len);
            if (k<len)
            {
                if(c2sqr * len <= k * k){
                    res.collides = true;
                    return res;
                }
            }
        }

        /*;
        Third edge*/

        if (k > 0)
        {
            double len = e3x * e3x + e3y * e3y;
            len = Math.sqrt(len);
            if (k<len)
            {
                if(c3sqr * len <= k * k){
                    res.collides = true;
                    return res;
                }
            }
        }

        res.collides = false;
        return res;
    }
}
