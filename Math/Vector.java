package Math;

/**
 * Created by niels on 08/04/2017.
 */
public class Vector {
    public double x,y;
    public Vector(double x, double y) {
        this.x=x;
        this.y=y;
    }

    public Vector add (Vector v) {
        return new Vector(v.x + x, v.y + y);
    }

    public Vector substract (Vector v) {
        return new Vector(v.x - x, v.y - y);
    }

    public Vector scale(double s) {
        return new Vector(this.x*s, this.y*s);
    }

    public double dot(Vector v) {
        return this.x * v.x + this.y * v.y;
    }

    public double cross(Vector v) {
        return (this.x * v.y - this.y * v.x);
    }

    public Vector rotate(Vector point, double angle) {
        double rotationX = this.x - point.x;
        double rotationY = this.y - point.y;

        double x_prime = point.x + ((rotationX * Math.cos(angle)) - (rotationY * Math.sin(angle)));
        double y_prime = point.y + ((rotationX * Math.sin(angle)) + (rotationY * Math.cos(angle)));

        return new Vector(x_prime, y_prime);
    }
}
