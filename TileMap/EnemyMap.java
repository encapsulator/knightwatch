package TileMap;

import Entity.CompositeEntity;
import Entity.MapEntity;
import Main.Settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.util.Arrays;

/**
 * Created by niels on 20/04/2017.
 */
public class EnemyMap {

    private static CompositeEntity enemies;
    private static EnemyMap instance;

    public static EnemyMap getInstance() {
        if (instance == null) {
            instance = new EnemyMap();
        }
        return instance;
    }

    public EnemyMap() {
        enemies = new CompositeEntity();
    }

    public void loadEnemies() throws NumberFormatException, IOException {
        enemies.getEntities().clear();
        InputStream stream = getClass().getResourceAsStream(Settings.ENEMYMAP);
        BufferedReader rdr = new BufferedReader(new InputStreamReader(stream));
        int width = Integer.parseInt(rdr.readLine());
        int height = Integer.parseInt(rdr.readLine());
        String delimiter = "\\s+";
        for (int row = 0; row != height; row++) {
            String r = rdr.readLine();
            String[] rowElements = r.split(delimiter);
            for (int col = 0; col != width; col++) {
                int element = Integer.parseInt(rowElements[col]);
                MapEntity newEnemy = null;
                if (element != 0) {
                    Class<? extends MapEntity> clazz = Arrays.stream(EnemyEnum.values()).filter(o -> o.getEnemyType() == element).findFirst().get().getClazz();
                    try {
                        Constructor<?> constructor = clazz.getConstructor(double.class, double.class, TileMap.class);
                        Object newInstance = constructor.newInstance(col * Settings.TILESIZE, row * Settings.TILESIZE, TileMap.getInstance());
                        newEnemy = (MapEntity) newInstance;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    enemies.add(newEnemy);
                }
            }
        }
    }

    public static CompositeEntity getEnemies() throws NumberFormatException, IOException {
        return enemies;
    }

}
