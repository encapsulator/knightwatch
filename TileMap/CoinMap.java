package TileMap;

import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import Entity.Coin;
import Main.Settings;
import Shape.Shape;

public class CoinMap {

	private static Set<Coin> coins=new HashSet<>();
	private static Coin[][] coinMap;
	private static CoinMap instance;
	
	public static CoinMap getInstance(){
		if(instance==null){
			instance = new CoinMap();
		}
		return instance;
	}
	
	
	public void loadCoins() throws NumberFormatException, IOException{
		InputStream stream = getClass().getResourceAsStream(Settings.COINMAP);
        BufferedReader rdr = new BufferedReader(new InputStreamReader(stream));
        int width = Integer.parseInt(rdr.readLine());
        int height = Integer.parseInt(rdr.readLine());
        coinMap = new Coin[height][width];
        String delimiter = "\\s+";
        for(int row=0;row!=height;row++){
        	String r = rdr.readLine();
        	String[] rowElements = r.split(delimiter);
        	for(int col=0;col!=width;col++){
        		int element = Integer.parseInt(rowElements[col]); 
        		Coin newCoin = null;
        		if(element != 0){
        			newCoin = new Coin(col*Settings.COINTILESIZE, row*Settings.COINTILESIZE);
        			coins.add(newCoin);
        		}
        		
        		coinMap[row][col] = newCoin;
        	}
        }
	}
	
	public static ArrayList<Coin> getCloseCoins(int row, int col){
		
		int mrow=row+8<=coinMap.length?row+8:coinMap.length;
		mrow = mrow<0?0:mrow;
		int mcol=col+8<=coinMap[0].length?col+8:coinMap[0].length;
		mcol = mcol<0?0:mcol;
		ArrayList<Coin> coinz = new ArrayList<Coin>();
		if(col>=0 && row>=0 && col<=coinMap[0].length && row <= coinMap.length)
		for(row=row-8>=0?row-8:0;row!=mrow;row++){
			for(col=col-8>=0?col-8:0;col!=mcol;col++){
				Coin e = coinMap[row][col];
				if(e!=null){
					coinz.add(e);
				}
			}
		}
		
		return coinz;
	}
	
	public static void removeCoin(Coin c){
		coins.remove(c);
		Shape s = c.getShape();
		coinMap[(int)s.position.y/Settings.COINTILESIZE][(int)s.position.x/Settings.COINTILESIZE]=null;
	}
	
	public static Set<Coin> getCoins() throws NumberFormatException, IOException{
        return coins;
	}
	
	public static void draw(Graphics2D g){
		for(Coin e:coins){
			if(e.isVisible()){
				e.draw(g);
			}
		}
	}
	 
}
