/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TileMap;

import GameState.Loadable;
import Main.Camera;
import Main.ImageLoader;
import Main.Settings;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author niels
 */
public class TileMap implements Loadable {

	private final int TILESIZE = 64;

	public int height, width;
	private BufferedImage tileset;
	private static Tile[][] tiles;
	private static int[][] tileMap;
	private static int horTiles;
	private int verTiles;
	private int mapHorTiles, mapVerTiles;
	private int offsetX = 0;
	private static TileMap instance;

	public TileMap() {

	}

	public static int getSType(int row, int col){
		int code = tileMap[row][col];
		int result = -1;
		if (code != 0) {
			int r = (int) code / horTiles;
			int c = (int) code % horTiles;
			if (c == 0) {
				c = horTiles - 1;
				r -= 1;
			} else {
				c -= 1;
			}

			result =  tiles[c][r].getType();
		}
		return result;
	}


	public static TileMap getInstance() {
		if (instance == null) {
			instance = new TileMap();
			return instance;
		}
		return instance;
	}
	//load tileset


	@Override
	public void load() {
		try {

			tileset = ImageLoader.getInstance().loadImage(Settings.TILESET);
			int tileSetWidth = tileset.getWidth();
			int tileSetHeight = tileset.getHeight();
			horTiles = tileSetWidth / TILESIZE;
			verTiles = tileSetHeight / TILESIZE;

			BufferedImage subImage;
			tiles = new Tile[horTiles][verTiles];
			for (int i = 0; i != horTiles; i++) {
				for (int j = 0; j != verTiles; j++) {

					int posx = i * TILESIZE;
					int posy = j * TILESIZE;
					subImage = tileset.getSubimage(posx, posy, TILESIZE,
							TILESIZE);
					subImage = ImageLoader.getInstance().loadImage(subImage);
					subImage = ImageLoader.getInstance().loadNoTransparancy(subImage);
					if (j <= 3) {
						tiles[i][j] = new Tile(subImage, Tile.BLOCKED);
					}else
					if (j == 4 && (i == 1 || i==0) ) {
						tiles[i][j] = new Tile(subImage, Tile.SLOPEUP);
					}else
					if(j == 4 && (i==3||i==2)){
						tiles[i][j] = new Tile(subImage, Tile.SLOPEDOWN);
					}else{
						tiles[i][j] = new Tile(subImage, Tile.NOCOLISSION);
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadTileMap(String s) {
		InputStream stream = getClass().getResourceAsStream(s);
		BufferedReader rdr = new BufferedReader(new InputStreamReader(stream));

		try {
			mapHorTiles = Integer.parseInt(rdr.readLine());
			mapVerTiles = Integer.parseInt(rdr.readLine());
			width = mapHorTiles * TILESIZE;
			height = mapVerTiles * TILESIZE;
			tileMap = new int[mapVerTiles][mapHorTiles];
			String delimiter = "\\s+";
			for (int i = 0; i != mapVerTiles; i++) {
				String str = rdr.readLine();
				String[] line = str.split(delimiter);
				for (int j = 0; j != mapHorTiles; j++) {
					tileMap[i][j] = Integer.parseInt(line[j]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int[] getNumTiles() {
		int[] res = new int[2];
		res[0] = mapVerTiles;
		res[1] = mapHorTiles;
		return res;
	}

	public int[][] getMap() {
		return tileMap;
	}

	public int getTileSize() {
		return TILESIZE;
	}

	public int getType(int row, int col) {

		int rc = tileMap[row][col];

		if (rc != 0) {
			int r = rc / horTiles;
			int c = rc % horTiles;
			if (c == 0) {
				c = horTiles - 1;
				r -= 1;
			} else {
				c -= 1;
			}

			return tiles[c][r].getType();
		}
		return -1;
	}

	public void draw(Graphics2D g) {
		for (int rows = 0; rows != mapVerTiles; rows++) {
			int cols = (int) ((-Camera.scrollX / TILESIZE) - 5);
			cols = cols < 0 ? 0 : cols;
			int maxCols = (int) (((-Camera.scrollX + Settings.GAMEWIDTH) / TILESIZE) +5);
			maxCols = maxCols > mapHorTiles ? mapHorTiles - 1 : maxCols;
			for (; cols < maxCols; cols++) {
				int code = tileMap[rows][cols];

				if (code == 0)
					continue;
				int r = code / horTiles;
				int c = code % horTiles;
				if (c == 0) {
					c = horTiles - 1;
					r -= 1;
				} else {
					c -= 1;
				}

				g.drawImage(tiles[c][r].getImage(), offsetX + cols * TILESIZE,
						rows * TILESIZE, null);
			}

		}
	}
}