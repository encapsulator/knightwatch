package TileMap;

import Entity.MapEntity;
import Entity.Saw;
import Entity.SkeletonEnemy;

/**
 * Created by niels on 20/04/2017.
 */
public enum EnemyEnum {

    SAW(1, Saw.class),
    SKELETON(2, SkeletonEnemy.class);

    private int enemyType;
    private Class<? extends MapEntity> clazz;

    EnemyEnum(int enemyType, Class<? extends MapEntity> clazz) {
        this.enemyType = enemyType;
        this.clazz = clazz;
    }

    public int getEnemyType() {
        return enemyType;
    }

    public Class<? extends MapEntity> getClazz() {
        return clazz;
    }
}
