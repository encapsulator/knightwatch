/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TileMap;

import java.awt.image.BufferedImage;

/**
 *
 * @author niels
 */
public class Tile {
    
    private BufferedImage image;
    private int type;
    
    public static final int NOCOLISSION=0;
    public static final int BLOCKED=1;
    public static final int SLOPEUP=2;
    public static final int SLOPEDOWN=3;
    
    public Tile(BufferedImage image, int type){
        this.image = image;
        this.type = type;
    }
    
    public void setTile(BufferedImage image){this.image=image;}
    public BufferedImage getImage(){return image;}
    public int getType() { return type; }
}
