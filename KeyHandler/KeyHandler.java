package KeyHandler;

import java.awt.event.KeyEvent;


public class KeyHandler {

	public static final int NUM_KEYS = 11;

	public static boolean keyState[] = new boolean[NUM_KEYS];

	public static int UP = 0;
	public static int LEFT = 1;
	public static int DOWN = 2;
	public static int RIGHT = 3;
	public static int ENTER = 4;
	public static int ESCAPE = 5;
	public static int SPACE = 6;
	public static int CTRL = 7;
	public static int ALT = 8;
	public static int RESET = 9;
	public static int COMMAND = 10;

	public static void keySet(int i, boolean b) {
		if(i == KeyEvent.VK_UP) keyState[UP] = b;
		else if(i == KeyEvent.VK_LEFT) keyState[LEFT] = b;
		else if(i == KeyEvent.VK_DOWN) keyState[DOWN] = b;
		else if(i == KeyEvent.VK_RIGHT) keyState[RIGHT] = b;
		else if(i == KeyEvent.VK_ENTER) keyState[ENTER] = b;
		else if(i == KeyEvent.VK_ESCAPE) keyState[ESCAPE] = b;
		else if(i == KeyEvent.VK_SPACE) keyState[SPACE] = b;
		else if(i==KeyEvent.VK_CONTROL)keyState[CTRL] = b;
		else if(i==KeyEvent.VK_ALT)keyState[ALT]=b;
		else if(i==KeyEvent.VK_R)keyState[RESET]=b;
		else if(i==KeyEvent.VK_META) keyState[COMMAND]=b;
	}


	public static boolean isPressed(int i) {
		return keyState[i];
	}

}
