/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GameState;

import java.awt.Graphics2D;
import java.io.IOException;

/**
 *
 * @author niels
 */
public class GameStateManager {
    private GameState[] gameStates;
    private boolean paused;
    private int activeState;
    public static final int LEVEL=0;
    public static final int MENU=1;
    
    public GameStateManager(int numStates) throws NumberFormatException, IOException{
        gameStates = new GameState[numStates];
        gameStates[0] = new LevelState(this);
        gameStates[1] = new MenuState(this);
        
        setState(1);
        paused = false;
    }

    public GameStateManager() throws NumberFormatException, IOException {
        gameStates = new GameState[2];
        gameStates[LEVEL] = new LevelState(this);
        gameStates[MENU] = new MenuState(this);
        
        setState(1);
        paused=false;
    }
    
    public void pause(){
        paused = true;
    }
    
    public void run(){
        paused = false;
    }
    
    public void setState(int s){activeState = s;}
    
    public void update(){
        if(!paused)this.gameStates[activeState].update();
    }
    
    public void draw(Graphics2D g){
        if(!paused)this.gameStates[activeState].draw(g);
    }

    public GameState getState(){
    	return this.gameStates[activeState];
    }
    
}
