/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GameState;

import Entity.*;
import KeyHandler.KeyHandler;
import Main.Background;
import Main.Camera;
import Main.Settings;
import TileMap.*;

import java.awt.*;
import java.io.IOException;
import java.util.Set;

import static KeyHandler.KeyHandler.*;

/**
 *
 * @author niels
 */
public class LevelState extends GameState {

	private Player player;
	private CoinMap coinmap;
	private EnemyMap enemyMap;

	private Set<Coin> coins;
	private TileMap tm;
	private boolean reset = false;
	public static boolean reloading = false;
	private LevelUI levelUI;

	public LevelState(GameStateManager manager) throws NumberFormatException, IOException {
		super(manager);
		levelUI = LevelUI.getInstance();
		coinmap = new CoinMap();
		enemyMap = EnemyMap.getInstance();
		load();

	}

	@Override
	public void draw(Graphics2D g) {


		Background.getInstance().draw(g);

		player.draw(g);
		ParticleEngine.getInstance().draw(g);
		levelUI.draw(g);
	}

	@Override
	public void update() {
		reset = isPressed(RESET) && !reloading;
		Background.getInstance().update();

		// update walls later
		try {
			coins = CoinMap.getCoins();
		} catch (NumberFormatException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		for(Coin e:coins){
			if(e.isVisible()){
				e.update();
			}
		}

		player.update();


		if(player.isDead && !reloading){
			try {
				reset = true;
			} catch (NumberFormatException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		ParticleEngine.getInstance().update();

		if(reset){
			try {
				reset = false;
				reloading=true;
				levelUI.reset();
				Background.getInstance().reset();
				reload();
				player.paralize = true;
			} catch (NumberFormatException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		if(!Background.hasStarted){
			levelUI.resetPos();
			reloading=false;
			reset = false;
			Camera.scrollBack=false;
		}


	}

	public void loadMap(){
		tm = TileMap.getInstance();
		tm.load();
		tm.loadTileMap(Settings.TILEMAP);
	}

	public void loadPlayer(){
		player.load();
	}


	public void loadCoins() throws NumberFormatException, IOException{

		coins.clear();

		coinmap.loadCoins();
	}

	public void loadEnemies() {

		try {
			enemyMap.loadEnemies();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			player.setEnemies(EnemyMap.getEnemies());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void reload() throws NumberFormatException, IOException{
		if(Background.hasStarted){
			Camera.scrollBack=true;
			loadPlayer();
			loadEnemies();
			loadCoins();
		}

	}

	@Override
	public void load() {
		// TODO Auto-generated method stub
		loadMap();
		player = new Player(50,50,tm);
		loadEnemies();
		try {
			coinmap.loadCoins();
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
