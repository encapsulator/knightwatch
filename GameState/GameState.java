/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GameState;

import Main.Drawable;
import Main.Updateable;

/**
 *
 * @author niels
 */
public abstract class GameState implements Loadable, Drawable, Updateable{
    protected GameStateManager manager;
   
    public GameState(GameStateManager manager){
        this.manager = manager;
    }
   
}
