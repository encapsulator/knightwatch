package GameState;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import KeyHandler.KeyHandler;
import Main.Camera;

public class MenuState extends GameState {
	
	private ArrayList<String> options;
	private int focused = 1;
	private boolean enter, up, down;
	
	public MenuState(GameStateManager manager) {
		super(manager);
		options = new ArrayList<>();
		load();
	}

	@Override
	public void draw(Graphics2D g) {
		// TODO Auto-generated method stub
		
		for(int i=0;i!=options.size();i++){
			if(i+1 == focused){
				g.setColor(Color.RED);
				g.setFont(new Font("Verdana", Font.BOLD, 20));
				g.drawString(options.get(i), 415-options.get(i).length()/2, 300+i*40);
			} else {
				g.setColor(Color.BLACK);
				g.setFont(new Font("Verdana", Font.BOLD, 20));
				g.drawString(options.get(i), 415-options.get(i).length()/2, 300+i*40);
			}
		}
		g.setColor(Color.BLACK);
		g.setFont(new Font("Verdana", Font.BOLD, 20));
		g.drawString("JUMP: UP, SLASH: X, SHIELD: C, FLOAT: SPACE", 150, 500);
		g.drawString("FPS: F, RESET: R, CHOOSE: ENTER", 250, 540);
	}

	@Override
	public void load() {
		// TODO Auto-generated method stub

		options.add("Play");
		options.add("Settings");
	}

	@Override
	public void update() {
		enter = KeyHandler.keyState[KeyHandler.ENTER];
		up = KeyHandler.keyState[KeyHandler.UP];
		down = KeyHandler.keyState[KeyHandler.DOWN];
		// TODO Auto-generated method stub
		Camera.scrlSpeed = 0;
		if(enter && focused==1){
			this.manager.pause();
			this.manager.setState(GameStateManager.LEVEL);
			this.manager.run();
		}
		if(up){
			if(focused>=2){
				this.focused-=1;
			}else{
				this.focused = 1;
			}
		}
		if(down){
			if(focused<options.size()){
				this.focused+=1;
			}
		}
	}

}
