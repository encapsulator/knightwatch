package Main;

import Entity.LevelUI;
import GameState.GameStateManager;
import KeyHandler.KeyHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.URL;


@SuppressWarnings("serial")
public class GamePanel extends JPanel implements Runnable, KeyListener {

	// game thread
	private Thread thread;
	private boolean running;
	private int FPS = Settings.FPS;
	private long targetTime = 1000000000L / FPS;
	private FPSCounter fpsCounter = new FPSCounter();

	// game state manager
	private GameStateManager gsm;
	private JScrollPane scrollPane;

	public GamePanel() {
		super();
		setPreferredSize(new Dimension(Settings.WIDTH, Settings.HEIGHT));
		setFocusable(true);
		setBackground(Color.getColor("#4A2500"));
		requestFocus();

	}

	public void addNotify() {
		super.addNotify();
		if (thread == null) {
			thread = new Thread(this);
			addKeyListener(this);
			thread.start();
		}
	}

	private void init() {
		Camera.stopScroll();
		running = true;

		try {
			gsm = new GameStateManager(2);
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		URL[] a = new URL[4];
		a[0] = this.getClass().getClassLoader()
				.getResource("Resources/sounds/player/jump.wmv");
		a[1] = this.getClass().getClassLoader()
				.getResource("Resources/sounds/level/coin.wav");
		a[2] = this.getClass().getClassLoader()
				.getResource("Resources/sounds/player/attack.wav");
		a[3] = this.getClass().getClassLoader()
				.getResource("Resources/sounds/enemies/skeleton/die.wav");
		DiskJockey.putClips(a);
	}

	public void run() {
		init();

		long start;
		long elapsed;
		// game loop
		while (running) {
			start = System.nanoTime();
			fpsCounter.update();
			Camera.scroll(scrollPane);
			LevelUI.getInstance().update();

			update();

			scrollPane.getHorizontalScrollBar().setValue(scrollPane.getHorizontalScrollBar().getValue() + (int) Camera.scrollPixels);
			repaintAndWait();
			elapsed = System.nanoTime() - start;
			try {
				Thread.sleep((targetTime - elapsed) / 1000000 <= 0 ? 0 : (targetTime - elapsed) / 1000000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void update() {
		gsm.update();


	}

	public synchronized void repaintAndWait() {
		repaint();
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized void paintComponent(Graphics gr) {
		super.paintComponent(gr);
		Graphics2D g2d = (Graphics2D) gr;
		draw(g2d);
		notifyAll();
	}

	private void draw(Graphics2D g) {
		if (gsm != null) gsm.draw(g);
	}


	public void keyTyped(KeyEvent key) {
	}

	public void keyPressed(KeyEvent key) {
		KeyHandler.keySet(key.getKeyCode(), true);
	}

	public void keyReleased(KeyEvent key) {
		KeyHandler.keySet(key.getKeyCode(), false);
	}


	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
}
