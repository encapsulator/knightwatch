package Main;

import Entity.LevelUI;

import java.util.Timer;
import java.util.TimerTask;

public class FPSCounter
{

	private static short frames = 0;

	public FPSCounter() {

		TimerTask updateFPS = new TimerTask() {
			public void run() {
				// display current totalFrameCount - previous,
				// OR
				// display current totalFrameCount, then set
				LevelUI.getInstance().setFps(frames/5);
				frames = 0;
			}
		};
		Timer t = new Timer();
		t.scheduleAtFixedRate(updateFPS, 5000, 5000);
	}

	public final static void update()
	{
		frames++;
	}

}