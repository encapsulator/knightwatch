package Main;

import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageLoader {
	private GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
	private GraphicsDevice device = env.getDefaultScreenDevice();
	private GraphicsConfiguration config = device.getDefaultConfiguration();
	private static ImageLoader instance;
	
	public static ImageLoader getInstance(){
		if(instance==null){
			instance = new ImageLoader();
		}
		return instance;
	}
	
	public BufferedImage loadNoTransparancy(BufferedImage f){
		BufferedImage optimalImage = config.createCompatibleImage(f.getWidth(null), f.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics tempGraphics = optimalImage.getGraphics();

		tempGraphics.drawImage(f, 0, 0, null);
		tempGraphics.dispose();
		return optimalImage;
	}
	
	public BufferedImage load(BufferedImage f){
			BufferedImage optimalImage = config.createCompatibleImage(f.getWidth(null), f.getHeight(null), Transparency.TRANSLUCENT);
			Graphics tempGraphics = optimalImage.getGraphics();

			tempGraphics.drawImage(f, 0, 0, null);
			tempGraphics.dispose();
			return optimalImage;
	}
	
	public BufferedImage loadImage(String filename){
		BufferedImage image = null;
		try {
			image = ImageIO.read(this.getClass().getResource(filename));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return load(image);
	}
	
	public BufferedImage loadImage(BufferedImage file){
		return load(file);
	}
	
	BufferedImage flipHorizontally(BufferedImage img){
		 AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
		    tx.translate(-img.getWidth(null), 0);
		    AffineTransformOp op = new AffineTransformOp(tx,
		        AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		    return load(op.filter(img, null));
	}

	
}
