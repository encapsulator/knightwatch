package Main;

import java.awt.Color;
import java.awt.Dimension;


import javax.swing.*;

public class Main {

	public static void main(String[] args) {

		Dimension resolution = new Dimension(Settings.GAMEWIDTH,Settings.HEIGHT+26);
		JFrame win = new JFrame();

		GamePanel panel = new GamePanel();

		
		final JScrollPane scrollPane = new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

	
		scrollPane.setWheelScrollingEnabled(false);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(1);
		disableArrowKeys(scrollPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT));

		scrollPane.setPreferredSize(resolution);
		scrollPane.setSize(resolution);
		scrollPane.setBackground(Color.getColor("#4A2500"));


		scrollPane.getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);

		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setContentPane(scrollPane);
		win.setPreferredSize(resolution);
		win.setSize(resolution);


		panel.setScrollPane(scrollPane);
		win.setVisible(true);
	}

	private static void disableArrowKeys(InputMap im) {
		String[] keystrokeNames = { "UP", "DOWN", "LEFT", "RIGHT" };
		for (int i = 0; i < keystrokeNames.length; ++i) {
			im.put(KeyStroke.getKeyStroke(keystrokeNames[i]), "none");
		}
	}
}