package Main.BackgroundState;

import Main.Background;
import Main.Camera;

public class RewindState extends BackgroundState {

	@Override
	public void update() {
		// TODO Auto-generated method stub
		Background b = Background.getInstance();

		if (Background.hasStarted) {
			b.posStat -= Camera.scrollBackStep;
		}

		if (-Camera.scrollX <= b.posFast - b.backgroundFast.getWidth()) {


			if (Background.hasStarted) {
				b.posFast -= b.backgroundFast.getWidth() * 2;
			}
		}
		if (-Camera.scrollX <= b.posFastR - b.backgroundFast.getWidth()) {

			if (Background.hasStarted) {
				b.posFastR -= b.backgroundFastR.getWidth() * 2;

			}
		}
		if (-Camera.scrollX <= b.posSlow - b.backgroundSlow.getWidth()) {
			b.posSlow -= b.backgroundSlow.getWidth() * 2;
		}
		if (-Camera.scrollX <= b.posSlowR - b.backgroundSlow.getWidth()) {
			b.posSlowR -= b.backgroundSlow.getWidth() * 2;
		}

		b.posStat = b.posStat < -60 ? -60 : b.posStat;

	}

}
