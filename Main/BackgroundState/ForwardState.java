package Main.BackgroundState;

import Main.Background;
import Main.Camera;

public class ForwardState extends BackgroundState {
	@Override
	public void update(){
		Background b = Background.getInstance();

			if (Background.hasStarted) {
				b.posStat += -Camera.scrlSpeed;
				
			}

			if (-Camera.scrollX >= b.posFast + b.backgroundFast.getWidth()) {
				
				if (Background.hasStarted) {
					b.posFast += b.backgroundFast.getWidth()*2;
					
				}
			}
			if (-Camera.scrollX >= b.posFastR + b.backgroundFastR.getWidth()  ) {
				
				if (Background.hasStarted) {
					b.posFastR += b.backgroundFastR.getWidth()*2;
					
				}
			}
			if (-Camera.scrollX >= b.posSlow + b.backgroundSlow.getWidth()) {
				b.posSlow += b.backgroundSlow.getWidth()*2;
			}
			if (-Camera.scrollX >= b.posSlowR + b.backgroundSlowR.getWidth()) {
				b.posSlowR += b.backgroundSlow.getWidth()*2;
			}

	}
}
