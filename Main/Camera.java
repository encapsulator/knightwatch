package Main;

import javax.swing.*;

import static Main.Settings.*;

public class Camera {
	public static double scrollX=0;
	public static double scrlSpeed = 0;
	public static boolean scrollBack = false;
	public static double scrollBackStep = 0;
	public static double scrollPixels = 0;
	private static Camera instance;

	public static Camera getInstance(){
		if(instance==null)
			instance = new Camera();
		return instance;
	}

	public static void scroll(JScrollPane scrollPane){

		if(!scrollBack){
			if(-scrollX >= Settings.WIDTH-Settings.GAMEWIDTH || !Background.hasStarted){
				scrlSpeed = 0;
				scrollPixels = 0;

			} else {
				scrollX+=scrlSpeed;
				scrollPixels = -scrlSpeed;
			}
		}else{
			if(scrollBackStep==0){
				scrollBackStep = (-scrollX/SCROLLBACKTIME < MINREWINDSCROLLSPEED)?MINREWINDSCROLLSPEED:(int)-scrollX/SCROLLBACKTIME;
				scrollBackStep = scrollBackStep >= MAXREWINDSCROLLSPEED?MAXREWINDSCROLLSPEED:scrollBackStep;
			}
			if(scrollX+scrollBackStep<=0){
				scrollPixels = -scrollBackStep;
				scrollX+=scrollBackStep;
			}else{
				scrollX = 0;
				scrollBackStep=0;
				scrlSpeed =0;
				scrollPixels = 0;
				scrollPane.getHorizontalScrollBar().setValue(0);
			}
		}

	}

	public static void resetScrollSpeed(){
		scrlSpeed = SCROLLFORWARDSPEED;
	}

	public static  void stopScroll(){
		scrlSpeed=0;
	}
}
