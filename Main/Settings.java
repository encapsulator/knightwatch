package Main;

/**
 * Created by niels on 21/04/2017.
 */
public class Settings {

    //PLAYER
    public static final int PLAYERSHAPEWIDTH = 50;
    public static final int PLAYERSHAPEHEIGHT = 96;
    public static final int HURTDELAY = 70,
            RESETAFTERDEADDELAY = 120,
            GETALIVEDELAY = 30;
    public static int HEALTH = 100;
    public static int PLAYERGOTHITPUSHBACK = 5;

    //GAME PHYSICS
    public static final double SPEEDY = 0.1;
    public static final double GRAVITY = 0.9;
    public static final double JUMPPUSH = 17;
    public static final double DBLJUMPFACTOR = 1.4;
    public static final double FRICTION = 0.9;
    public static final double MAXX = 2.5;
    public static final double MAXY = 20;
    public static final double MINX = 2;
    public static final double MAXFLYFALLSPEED = 5;
    public static final int TOTALJUMPS = 2;
    public static final int ATTACKRECHARGETIME = 30;
    public static final int MAXCROUCHSPEEDDIVISOR = 2;

    //SKELETON
    public static final int ATTACKDELAY = 100;
    public static final int SWORDHITDAMAGE = 25;
    public static final int HITBOXWIDTH = 150;
    public static final int FLYBACKTIME = 100;
    public static final int DIETIMEOUT = 62 * 4;
    public static final int HITONSHIELDTIMEOUT = 120;
    public static final int DISAPPEARAFTERDEADTIMEOUT = 60 * 3;
    public static final int DEADSWITCHSHOWTIMEOUT = 20;
    public static final double FLYBACKDECELERATE = 0.1;
    public static final double FLYBACKWIDTH = 40;
    public static final int ATTACTMAXCOUNTDOWNTIME = 75;

    //SAW
    public static final int ROTATIONTIME = 3;
    public static final double ROTATIONPERTIME = 45/10;

    //CAMERA
    public static final int SCROLLFORWARDSPEED = -1;
    public static final int MINREWINDSCROLLSPEED = 5;
    public static final int MAXREWINDSCROLLSPEED = 15;
    public final static int SCROLLBACKTIME = 300;

    //LEVEL & LEVEL UI
    public static final String COINMAP = "/Resources/scenes/testLevel/coinmap";
    public static final String ENEMYMAP = "/Resources/scenes/testLevel/enemymap";
    public static final String TILEMAP = "/Resources/scenes/testLevel/tilemap";
    public static final String TILESET = "/Resources/sprites/map/tileset.png";

    public static final int TILESIZE = 64;
    public static final int STATSX = 25;
    public static final int STATSY = 0;
    public static final int COINSTXTX = 110 + STATSX;
    public static final int COINTXTY = 53;
    public static final int COINTILESIZE= 32;
    public static final int HEALTHTXTX = COINSTXTX;
    public static final int HEALTHTXTY = 95;
    public static final int FPSTXTX = 840;
    public static final int FPSTXTY = 20;
    public static final int COINVALUE = 5;

    //GAME PANEL
    public static final int WIDTH = 103 * 64;
    public static final int HEIGHT = 9 * 64;
    public static final int GAMEWIDTH = 900;
    public static final int FPS = 68;

    //SPRITES
    public static final String PLAYER_WALK = "/Resources/sprites/player/walk.png";
    public static final String PLAYER_RUN = "/Resources/sprites/player/run.png";
    public static final String PLAYER_JUMP = "/Resources/sprites/player/jump.png";
    public static final String PLAYER_IDLE = "/Resources/sprites/player/idle.png";
    public static final String PLAYER_CROUCH = "/Resources/sprites/player/crouch.png";
    public static final String PLAYER_ATTACK = "/Resources/sprites/player/attack.png";
    public static final String PLAYER_JUMP_ATTACK = "/Resources/sprites/player/attackInAir.png";
    public static final String PLAYER_DEFEND = "/Resources/sprites/player/defend.png";
    public static final String PLAYER_CROUCH_WALK = "/Resources/sprites/player/crouchWalk.png";
    public static final String PLAYER_HURT = "/Resources/sprites/player/hurt.png";
    public static final String PLAYER_DIE = "/Resources/sprites/player/die.png";

    public static final String SKELETON_IDLE = "/Resources/sprites/enemies/skeleton/idle.png";
    public static final String SKELETON_ATTACK = "/Resources/sprites/enemies/skeleton/attack.png";
    public static final String SKELETON_CHARGE = "/Resources/sprites/enemies/skeleton/charge.png";
    public static final String SKELETON_DIE = "/Resources/sprites/enemies/skeleton/die.png";

    public static final String BGSLOWGREEN = "/Resources/sprites/level/background/green/bgForest.png";
    public static final String BGFASTGREEN = "/Resources/sprites/level/background/green/bg.png";
    public static final String BGSTATGREEN = "/Resources/sprites/level/background/green/bgMoon.png";

    public static final String BGSLOWBROWN = "/Resources/sprites/level/background/brown/bgForest.png";
    public static final String BGFASTBROWN = "/Resources/sprites/level/background/brown/bg.png";
    public static final String BGSTATBROWN = "/Resources/sprites/level/background/brown/bgMoon.png";

    public static final String SAW = "/Resources/sprites/enemies/saw/saw.png";

    public static final String LVLSTATS = "/Resources/sprites/level/windows/statistics.png";

    public static final String COIN = "/Resources/sprites/level/entities/coins.png";
}
