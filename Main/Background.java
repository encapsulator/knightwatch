package Main;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import Main.BackgroundState.BackgroundState;
import Main.BackgroundState.ForwardState;
import Main.BackgroundState.RewindState;

public class Background implements Updateable, Drawable {
    public BufferedImage backgroundSlow, backgroundSlowR;
    public BufferedImage backgroundFast, backgroundFastR;
    public BufferedImage backgroundStatic;

    public double posSlow = 0;
    public double posSlowR;
    public double posFast = 0;
    public double posFastR;
    public double posStat = -60;
    public static boolean hasStarted;
    private static Background instance;
    private BackgroundState bgState;

    public Background() {
        bgState = new ForwardState();
        backgroundSlow = ImageLoader.getInstance().loadImage(Settings.BGSLOWGREEN);
        backgroundSlow = ImageLoader.getInstance().loadImage(backgroundSlow);
        backgroundSlowR = ImageLoader.getInstance().flipHorizontally(backgroundSlow);
        backgroundSlowR = ImageLoader.getInstance().loadNoTransparancy(backgroundSlowR);
        backgroundFast = ImageLoader.getInstance().loadImage(Settings.BGFASTGREEN);
        backgroundFast = ImageLoader.getInstance().loadNoTransparancy(backgroundFast);
        backgroundFastR = ImageLoader.getInstance().flipHorizontally(backgroundFast);
        backgroundFastR = ImageLoader.getInstance().loadNoTransparancy(backgroundFastR);
        backgroundStatic = ImageLoader.getInstance().loadImage(Settings.BGSTATGREEN);
        posSlowR = backgroundSlowR.getWidth();
        posFastR = backgroundFastR.getWidth();
    }

    public static Background getInstance() {
        if (instance == null) {
            instance = new Background();
        }
        return instance;
    }

    public void setSpeed(double playermove) {
        if (hasStarted) {
            posSlow -= playermove / 4;
            posSlowR -= playermove / 4;
        }
    }

    public static void setHasStarted(boolean b) {
        hasStarted = b;
    }

    public void reset() {
        bgState = new RewindState();
    }

    public void moveForward() {
        bgState = new ForwardState();
    }

    @Override
    public void draw(Graphics2D g) {
        AffineTransform stat = new AffineTransform();
        stat.translate(posStat, 0); // x/y set here, ball.x/y = double, ie: 10.33

        AffineTransform slow = new AffineTransform();
        slow.translate(posSlow, 0); // x/y set here, ball.x/y = double, ie: 10.33

        AffineTransform slowR = new AffineTransform();
        slowR.translate(posSlowR, 0); // x/y set here, ball.x/y = double, ie: 10.33

        g.drawImage(backgroundFast, (int) posFast, 0, null);

        g.drawImage(backgroundFastR,
                (int) posFastR, 0, null);


        g.drawImage(backgroundStatic, stat, null);

        g.drawImage(backgroundSlow, slow, null);
        g.drawImage(backgroundSlowR, slowR, null);

    }

    @Override
    public void update() {
        this.bgState.update();
        if (-Camera.scrollX <= 0 && this.bgState instanceof RewindState) {
            setHasStarted(false);
            moveForward();
            Camera.scrollX = 0;
            posStat = -60;
        }

    }

}
