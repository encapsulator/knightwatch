package Main;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.IOException;
import java.util.ArrayList;
import java.net.URL;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class DiskJockey {
	public static ArrayList<URL> audio = new ArrayList<>();
	public static ArrayList<AudioClip> clips = new ArrayList<>();
	public static final int JUMP = 0;
	public static final int COIN =1;
	public static final int ATTACK =2;
	public static final int SKELETONHURT = 3;
	public static ArrayList<Boolean> isPlaying = new ArrayList<>();
	
	public static void putClips(URL[] s){
		for(int i=0;i!=s.length;i++){
			AudioClip clip = Applet.newAudioClip(
					s[i]);
			isPlaying.add(false);
			clips.add(clip);
		}

	}


	public static void playClip(int number, boolean loop) throws IOException,
	UnsupportedAudioFileException, LineUnavailableException, InterruptedException {

		AudioClip clip = clips.get(number);
		clip.play();
		}
}